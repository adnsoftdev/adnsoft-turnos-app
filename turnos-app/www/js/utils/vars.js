/**
 * @prop vars Objeto que contiene variables varias que se usan en la aplicación
 */
var vars = {
    url: 'http://clinicacp.ddns.net:5656',

    usuariosTurno: {
        idPacienteTitular: 0,
        nombrePacienteTitular: '',
        grupoPacienteTitular: []
    },

    usuarioLogueado: null,

    pacienteTemp: null,

    especialidadTemp: {
        id: 0,
        nombre: ''
    },

    busquedaTipoActual: 0,

    tipoFiltroBusquedaPaciente: 0,

    pacienteAgenda: {
        pacienteId: 0,
        pacienteNombre: '',
        agendaTipo: 0,
        agendaId: 0,
        agendaNombre: '',
        agendaDias: '',
        observaciones: ''
    },

    diasSemana: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],

    /**
     * agendaId 
     * agendaTipo 
     * agendaNombre 
     * agendaObservaciones
     * pacienteId 
     * pacienteNombre
     * minimoHorarioDias: [{dia, hora}]
     */
    pacientesTurno: [],

    pacientesEnviar: {
        desde: null,
        data: []
    },

    quincenaDesde: '',
    quincenaHasta: '',
    nroQuincena: 0,
    horaDesde: null,
    horaHasta: null,
    ultimoHorarioAsignado: {
        fh: null,
        p: null
    },

    selFechaElegida: '',

    reservaEnviar: {
        id: 0,
        data: []
    },

    fechaReservaTexto: '',

    turnosConsultados: [],

    tipoConsultaTurnos: 0,

    encuestaEnviar: {
        turnoId: 0,
        motivoConsulta: '',
        medicacion: ''
    },

    reiniciarDatos: function () {
        vars.nroQuincena = 0;
        vars.pacienteAgenda.agendaId = 0;
        vars.pacienteAgenda.pacienteId = 0;
        vars.pacienteAgenda.agendaNombre = '';
        vars.pacienteAgenda.agendaTipo = 0;
        vars.pacienteAgenda.pacienteNombre = '';
        vars.pacientesEnviar.data = [];
        vars.pacientesEnviar.desde = '';
        vars.pacientesTurno = [];
        vars.quincenaDesde = '';
        vars.quincenaHasta = '';
        vars.reservaEnviar.data = [];
        vars.fechaReservaTexto = '';
        vars.selFechaElegida = '';
        vars.usuariosTurno.grupoPacienteTitular = [];
        vars.especialidadTemp.id = 0;
        vars.especialidadTemp.nombre = '';
        vars.tipoFiltroBusquedaPaciente = 0;
        vars.turnosConsultados = [];
        vars.tipoConsultaTurnos = 0;
        vars.horaDesde = null;
        vars.horaHasta = null;
        vars.ultimoHorarioAsignado.fh = null;
        vars.ultimoHorarioAsignado.p = null;
        window.sessionStorage.clear();
    }
}
