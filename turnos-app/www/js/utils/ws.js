var ws = {
    /**
     * Llama al webservice para la función indicada.
     * @param {string} fn Función del webservice.
     * @param {string} mt Metodo http de la llamada
     * @param {any} dp Objeto que contiene los datos a subir al servidor.
     * @param {function} onCallError Función que se ejecuta cuando hay error de la llamada Ajax
     * @param {function} onWSSuccess Función que se ejecuta cuando el webservice devuelve los datos requeridos correctamente
     */
    call: function (fn, mt, dp, onCallError, onWSSuccess) {
        $.ajax({
            url: fn,
            data: JSON.stringify(dp),
            dataType: 'json',
            timeout: 20000,
            method: mt,
            contentType: 'application/json',
            error: function (jqXHR, textStatus, errorThrown) {
                if (onCallError != undefined && onCallError != null) {
                    onCallError(jqXHR, textStatus, errorThrown);
                }
            },
            success: function (d) {
                onWSSuccess(d);   
            }
        });
    }
}