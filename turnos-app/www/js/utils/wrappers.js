/**
 * Este archivo contiene objetos con funciones que manejan plugins, sin agregar mucho funcionamiento propio.
 * El propósito principal es el de controlar ciertas cosas relacionadas con las plataformas (por ej., los que
 * la plataforma browser no soporta) para que del lado del código de la app sea totalmente transparente. 
 */


/**
 * Funciones del plugin SpinnerPlugin
 */
var _loading = {
    on: function (msg) {
        if (device.platform != 'browser') {
            SpinnerPlugin.activityStart(msg, { dimBackground: true });
        }
    },

    off: function () {
        if (device.platform != 'browser') {
            SpinnerPlugin.activityStop();
        }
    }
}

var _appVersion = {
    get: function (callback) {
        if (device.platform != 'browser') {
            cordova.getAppVersion.getVersionNumber(function (versionNumber) {
                callback(versionNumber);
            })
        } else {
            callback('1.0.0');
        }
    }
}

var _toast = {
    /**
     * Muestra un mensaje toast en la pantalla.
     * @param {string} msg Texto a mostrar
     * @param {string} color color de fondo del toast
     * @param {string} tColor color del texto
     * @param {string} pos Posición del toast en la pantalla
     * @param {any} dur Duración del lector (número para indicarlo en milisegundos o texto para indicar "long" o "short")
     */
    show: function (msg, color, tColor, pos, dur) {
        if (device.platform != 'browser') {
            window.plugins.toast.showWithOptions({
                message: msg,
                duration: ((dur != null && dur != undefined && dur != '') ? dur : "long"),
                position: ((pos != null && pos != undefined && pos != '') ? pos : "bottom"),
                styling: {
                    opacity: 0.65,
                    backgroundColor: ((color != null && color != undefined && color != '') ? color : "#000000"),
                    textColor: ((tColor != null && tColor != undefined && tColor != '') ? tColor : "#ffffff"),
                    textSize: 16
                }
            });
        } else {
            alert(msg);
        }
    },
}

var _vibrator = {
    on: function (obj) {
        if (device.platform != 'browser') {
            navigator.vibrate(obj);
        }
    }
}

var _notification = {
    init: function (onClickCallback, onClearCallback) {
        this.setOnClickListener(onClickCallback);
        this.setOnClearListener(onClearCallback);
    },

    /**
     * Lanza una notificación
     * @param {number} id 
     * @param {string} tit 
     * @param {string} msg 
     * @param {boolean} mantener
     * @param {Array} actions
     * @param {any} data
     * @param {any} actions Objeto con las opciones que se van a mostrar al desplegar la notificación (Android >= 7)
     */
    show: function (id, tit, msg, mantener, actions, data) {
        var so = device.platform;
        var version = parseInt((device.version).slice(0, 1));
        var options = {};
        options.id = id;
        options.text = msg;
        options.title = tit;
        options.icon = 'file://img/icon/android/mipmap-hdpi/ic_launcher_android.png';
        options.smallIcon = 'res://icon/android/notification/ic_stat_app.png';
        options.ongoing = mantener;
        // options.sound = 'res://sound/appointed.mp3';
        
        // las opciones rápidas son compatibles desde Android 7 en adelante
        if (version >= 7) {
            if (actions != null) {
                options.actions = actions;
            }
            if (data != null) {
                options.data = data;
                options.group = 'pac' + data.pacienteId;
            }
        }
        // cordova.plugins.notification.local.getTriggered(function (notifications) {
            /*
            var hayGrupoPaciente = false;
            for (var i = 0; i < notifications.length; i++) {
                var notif = notifications[i];
                // var dataNotif = JSON.parse(notif.data);
                if (notif.groupSummary == true && notif.group == 'pac' + data.pacienteId) {
                    hayGrupoPaciente == true;
                }
            }
            if (!hayGrupoPaciente && notifications.length > 0) {
                // en el caso de que no exista el grupo buscado, lanzar una notificación de grupo
                cordova.plugins.notification.local.schedule({
                    id: (data.pacienteId*100), 
                    summary: data.pacienteNombre, 
                    group: 'pac' + data.pacienteId, 
                    groupSummary: true,
                    ongoing: false
                });
            }
            */
            // cordova.plugins.notification.local.clear(id, function () {
                cordova.plugins.notification.local.schedule(options);
            // });
            // if (onClickCallback != null && onClickCallback != undefined) {
            //     if (cordova.plugins.notification.local.core._listener.click != undefined) {
            //         cordova.plugins.notification.local.core._listener.click = null;
            //     }
            //     cordova.plugins.notification.local.on('click', function (notification, state) {
            //         if (notification.id == id) {
            //             onClickCallback();
            //         }
            //     }, this);
            // }
        // });
        
    },

    setOnClickListener: function (callback) {
        cordova.plugins.notification.local.on('click', function (notification, state) {
            callback(notification, state);
        });
    },

    setOnClearListener: function (callback) {
        cordova.plugins.notification.local.on('clear', function (notification) {
            callback(notification);
        });
    },

    configureAction: function (action, callback) {
        cordova.plugins.notification.local.on(action, function (notification, eopts) {
            callback();
        });
    },

    clearAll: function () {
        cordova.plugins.notification.local.clearAll(function () {
            // _toast.show('Notificaciones borradas', '', '', 'bottom', 'short');
        }, this);
    },

    /**
     * Reprograma una notificación por el tiempo especificado
     * @param {any} nt Notificación que se va a reprogramar
     * @param {number} time Tiempo a partir del momento actual que se va a lanzar la notificación (en milisegundos)
     * @param {Function} callback Función para ejecutar una vez que la notificación es reprogramada
     */
    showLater: function (nt, time, callback) {
        var repro = new Date(Date.now() + time);
        cordova.plugins.notification.local.schedule({
            id: nt.id, text: nt.text, title: nt.title,
            icon: nt.icon, smallIcon: nt.smallIcon, ongoing: nt.ongoing,
            actions: nt.actions, data: nt.data, group: nt.group,
            at: repro
        }, function () {
            if (callback != null && callback != undefined) {
                callback();
            }
        });
    }
}