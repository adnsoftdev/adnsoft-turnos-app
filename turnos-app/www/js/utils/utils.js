/**
 * Funciones para formatear fechas
 */
var uFecha = {
    procFechaSync: function (fec) {
        var f1 = fec.replace(/-/g, '');
        var f2 = f1.replace(' ', '');
        var f3 = f2.replace(/:/g, '');
        return f3;
    },

    /**
     * Convierte una fecha de tipo Date (javascript) y la convierte a string con el formato para guardar en la base
     *  (yyyy-MM-dd hh:mm:ss)
     * @param {Date} fecDate
     */
    getFechaStringDB: function (fecDate) {
        var fechaString = fecDate.getFullYear()
            + '-' + ((fecDate.getMonth() + 1) < 10 ? ('0' + (fecDate.getMonth() + 1)) : (fecDate.getMonth() + 1))
            + '-' + (fecDate.getDate() < 10 ? ('0' + fecDate.getDate()) : (fecDate.getDate()))
            + ' ' + (fecDate.getHours() < 10 ? ('0' + fecDate.getHours()) : (fecDate.getHours()))
            + ':' + (fecDate.getMinutes() < 10 ? ('0' + fecDate.getMinutes()) : (fecDate.getMinutes()))
            + ':' + (fecDate.getSeconds() < 10 ? ('0' + fecDate.getSeconds()) : (fecDate.getSeconds()));
        return fechaString;
    },

    /**
     * Convierte una fecha de tipo Date y la convierte a string con el formato yyyy-MM-dd
     * @param {Date} fecDate
     */
    getFechaStringDBsinHora: function (fecDate) {
        var fechaString = fecDate.getFullYear()
            + '-' + ((fecDate.getMonth() + 1) < 10 ? ('0' + (fecDate.getMonth() + 1)) : (fecDate.getMonth() + 1))
            + '-' + (fecDate.getDate() < 10 ? ('0' + fecDate.getDate()) : (fecDate.getDate()));
        return fechaString;
    },

    /**
     * Retorna un string con la fecha convertida en formato dd/MM/yyyy hh:mm:ss
     * @param {string} fecDate Fecha para formatear
     */
    getFechaString: function (fecDate) {
        var fec;
        if ((typeof fecDate) == 'string') {
            var fecFechaHoraSplit = fecDate.split(' ');

            var fecArray = fecFechaHoraSplit[0].split('-');
            var horaArray = fecFechaHoraSplit[1].split(':');
            fec = new Date(
                parseInt(fecArray[0]), (parseInt(fecArray[1]) - 1), parseInt(fecArray[2]),
                parseInt(horaArray[0]), parseInt(horaArray[1]), parseInt(horaArray[2])
            );
        } else {
            fec = fecDate;
        }
        var fechaString = (fec.getDate() < 10 ? ('0' + fec.getDate()) : (fec.getDate()))
            + '/' + ((fec.getMonth() + 1) < 10 ? ('0' + (fec.getMonth() + 1)) : (fec.getMonth() + 1))
            + '/' + fec.getFullYear()
            + ' ' + (fec.getHours() < 10 ? ('0' + fec.getHours()) : (fec.getHours()))
            + ':' + (fec.getMinutes() < 10 ? ('0' + fec.getMinutes()) : (fec.getMinutes()))
            + ':' + (fec.getSeconds() < 10 ? ('0' + fec.getSeconds()) : (fec.getSeconds()));
        return fechaString;
    },

    getFechaStringHoraCorta: function (fecDate) {
        var fec;
        if ((typeof fecDate) == 'string') {
            var fecFechaHoraSplit = fecDate.split(' ');

            var fecArray = fecFechaHoraSplit[0].split('-');
            var horaArray = fecFechaHoraSplit[1].split(':');
            fec = new Date(
                parseInt(fecArray[0]), (parseInt(fecArray[1]) - 1), parseInt(fecArray[2]),
                parseInt(horaArray[0]), parseInt(horaArray[1]), parseInt(horaArray[2])
            );
        } else {
            fec = fecDate;
        }
        var fechaString = (fec.getDate() < 10 ? ('0' + fec.getDate()) : (fec.getDate()))
            + '/' + ((fec.getMonth() + 1) < 10 ? ('0' + (fec.getMonth() + 1)) : (fec.getMonth() + 1))
            + '/' + fec.getFullYear()
            + ' ' + (fec.getHours() < 10 ? ('0' + fec.getHours()) : (fec.getHours()))
            + ':' + (fec.getMinutes() < 10 ? ('0' + fec.getMinutes()) : (fec.getMinutes()));
        return fechaString;
    },

    /**
     * Retorna una fecha sin la hora en formato dd/MM/yyyy
     * @param {string} fecDate 
     */
    getFechaSinHora: function (fecDate) {
        var fec;
        if ((typeof fecDate) == 'string') {
            var fecArray = fecDate.split('-');
            fec = new Date(parseInt(fecArray[0]), (parseInt(fecArray[1]) - 1), parseInt(fecArray[2]));
        } else {
            fec = fecDate;
        }
        var fechaString = (fec.getDate() < 10 ? ('0' + fec.getDate()) : (fec.getDate()))
            + '/' + ((fec.getMonth() + 1) < 10 ? ('0' + (fec.getMonth() + 1)) : (fec.getMonth() + 1))
            + '/' + fec.getFullYear();
        return fechaString;
    },

    /**
     * Retorna un objeto Date a partir de una fecha en string
     * @param {string} date Fecha en formato string (dd/MM/yyyy)
     * @returns {Date} Fecha de tipo Date
     */
    getFechaDate: function (date) {
        var fecArray = date.split('/');
        var dia = parseInt(fecArray[0]);
        var mes = parseInt(fecArray[1]);
        var anio = parseInt(fecArray[2]);
        var newDate = new Date(anio, (mes - 1), dia);
        return newDate;
    },

    /**
     * Retorna un objeto Date a partir de una fecha en string (con horas, minuntos y segundos)
     * @param {string} date Fecha en formato string (dd/MM/yyyy hh:mm:ss)
     * @returns {Date} Fecha de tipo Date
     */
    getFechaDateFull: function (date) {
        var fechaDiv = date.split(' ');
        var fechaArray = fechaDiv[0].split('/');
        var horaArray = fechaDiv[1].split(':');

        var dia = parseInt(fechaArray[0]);
        var mes = parseInt(fechaArray[1]);
        var anio = parseInt(fechaArray[2]);
        var hora = parseInt(horaArray[0]);
        var minuto = parseInt(horaArray[1]);
        var segundo = parseInt(horaArray[2]);

        var newDate = new Date(anio, (mes - 1), dia, hora, minuto, segundo);
        return newDate;
    },

    /**
     * Retorna un objeto Date a partir de una fecha string en formato yyyy-MM-dd HH:mm:ss
     * @param {string} fechaString Fecha de tipo string con el formato yyyy-MM-dd HH:mm:ss
     */
    getDateFromStringDb: function (fechaString) {
        var fechaDiv = fechaString.split(' ');
        var fechaArray = fechaDiv[0].split('-');
        var horaArray = fechaDiv[1].split(':');

        var dia = parseInt(fechaArray[2]);
        var mes = parseInt(fechaArray[1]);
        var anio = parseInt(fechaArray[0]);
        var hora = parseInt(horaArray[0]);
        var minuto = parseInt(horaArray[1]);
        var segundo = parseInt(horaArray[2]);

        var newDate = new Date(anio, (mes - 1), dia, hora, minuto, segundo);
        return newDate;
    },

    /**
     * Retorna una fecha en string en formato dd/MM/yyyy a partir de otro string en formato yyyy-MM-dd
     * @param {string} date Fecha string en formato yyyy-MM-dd
     */
    getFechaFormatoNormal: function (date) {
        var fecArray = date.split('-');
        return (fecArray[2] + '/' + fecArray[1] + '/' + fecArray[0]);
    },

    /**
     * Retorna una fecha en formato '[diaNombre], [dia] de [mes] de [año]' de tipo string
     * @param {Date} date Fecha que se quiere convertir
     */
    getFechaFormatoTexto: function (date) {
        var fechaTexto = '';
        switch (date.getDay()) {
            case 0:
                fechaTexto += 'Domingo';
                break;
            case 1:
                fechaTexto += 'Lunes';
                break;
            case 2:
                fechaTexto += 'Martes';
                break;
            case 3:
                fechaTexto += 'Miércoles';
                break;
            case 4:
                fechaTexto += 'Jueves';
                break;
            case 5:
                fechaTexto += 'Viernes';
                break;
            case 6:
                fechaTexto += 'Sábado';
                break;
        }
        fechaTexto += ', ' + date.getDate() + ' de ';
        switch (date.getMonth()) {
            case 0:
                fechaTexto += 'Enero';
                break;
            case 1:
                fechaTexto += 'Febrero';
                break;
            case 2:
                fechaTexto += 'Marzo';
                break;
            case 3:
                fechaTexto += 'Abril';
                break;
            case 4:
                fechaTexto += 'Mayo';
                break;
            case 5:
                fechaTexto += 'Junio';
                break;
            case 6:
                fechaTexto += 'Julio';
                break;
            case 7:
                fechaTexto += 'Agosto';
                break;
            case 8:
                fechaTexto += 'Septiembre';
                break;
            case 9:
                fechaTexto += 'Octubre';
                break;
            case 10:
                fechaTexto += 'Noviembre';
                break;
            case 11:
                fechaTexto += 'Diciembre';
                break;
        }
        fechaTexto += ' de ' + date.getFullYear();

        return fechaTexto;
    },

    /**
     * Devuelve una hora en formato string con el patrón "HH:mm" partiendo de otra hora en string con el patrón "HH:mm:ss" 
     * (o con zona horaria)
     * @param {string} hora Hora a formatear
     */
    getHoraCorta: function (hora) {
        var res1 = '';
        if (hora.indexOf('-') != -1) {
            res1 = hora.split('-')[0];
        } else {
            res1 = hora;
        }
        var res2 = res1.split(':');
        return (res2[0] + ':' + res2[1]);
    },

    diaValido: function (dia) {
        return (dia >= 1 && dia <= 31);
    },

    mesValido: function (mes) {
        return (mes >= 1 && mes <= 12);
    },

    anioValido: function (anio) {
        return (anio >= 1);
    },

    anioBisiesto: function (year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    },

    /**
     * Compara dos fechas en cuanto al año, mes y dia (sin tener en cuenta la hora)
     * @param {Date} fechaObj 
     * @param {Date} fechaNueva
     */
    mismaFecha: function (fechaObj, fechaNueva) {
        if (fechaObj.getFullYear() == fechaNueva.getFullYear()
            && fechaObj.getMonth() == fechaNueva.getMonth()
            && fechaObj.getDate() == fechaNueva.getDate()) {
            return true;
        } else {
            return false;
        }
    }
}


var uMensajes = {
    /**
     * Lanza un error del tipo especificado con el mensaje. Puede salir de la aplicación.
     * @param {string} msg Mensaje a mostrar
     * @param {number} tipo Tipo de mensaje: 1) Alert; 2) Toast rojo; 3) Vista de error
     * @param {boolean} salir Indica si al confirmar el error la aplicación se cierra
     */
    showError: function (msg, tipo, salir) {
        switch (tipo) {
            case 1:
                _vibrator.on([75, 75, 75, 75, 75, 75]);
                navigator.notification.alert(msg, function () {
                    if (salir) {
                        salir(false);
                    }
                }, 'Error', 'OK');
                break;
            case 2:
                if (device.platform != 'browser') {
                    _vibrator.on([75, 75, 75, 75, 75, 75]);
                    _toast.show(msg, '#990000', '', 'bottom', 'long');
                } else {
                    navigator.notification.alert(msg, function () {
                        if (salir) {
                            salir(false);
                        }
                    }, 'Error', 'OK');
                }
                break;
            case 3:
                //redirigir a la vista de error pasandole el mensaje
                break;
            default:
                break;
        }
    },

    /**
     * Muestra un aviso de que hay alguna función que no ha sido desarrollada todavía.
     */
    funcNoDisp: function () {
        _toast.show('Función no disponible', '#e65c00', '#ffffff', 'center', 'long');
    },

    getAppInfo: function () {
        cordova.getAppVersion.getVersionNumber(function (v) {
            $('#ver').text(v);
        });
        cordova.getAppVersion.getAppName(function (name) {
            $('#appName').text(name);
        });
    }
}

/**
 * Objeto con funciones para manejo de cadenas de texto
 */
var uCadenas = {
    procesarSqlParaEjecutar: function (cadena) {
        // var c = cadena.slice(8);
        // var c0 = c;
        // var c1 = c0.replace(/\\u0027/g, '\'');
        // var c2 = c1.replace(/--/g, '/*');
        // var c3 = c2.replace(/CREATE/g, '*/ CREATE');
        // var c4 = c3.replace(/\\t/g, '')

        var c = cadena.replace(/\r\n/g, ' ');
        return c;
    },

    /**
     * @param {string} numero
     */
    toLocalNumber: function (numero) {
        var numeroNuevo = parseFloat(numero).toLocaleString('es-AR', { maximumFractionDigits: 2, minimumFractionDigits: 2 });
        return numeroNuevo;
    },

    /**
     * Retorna el nombre completo de un paciente, en formato "APELLIDO APELLIDO2, NOMBRE NOMBRE2"
     * @param {any} paciente Paciente del que se desea obtener el nombre completo
     */
    getPacienteNombreCompleto: function (paciente) {
        var pacienteNombreCompleto = '';
        pacienteNombreCompleto =
            paciente.apellido +
            (paciente.oApellido != '' ? (' ' + paciente.oApellido) : ('')) +
            ', ' +
            paciente.nombre +
            (paciente.oNombre != '' ? (' ' + paciente.oNombre) : '');
        return pacienteNombreCompleto;
    },

    /**
     * A partir de una cadena con los días de disponibilidad de agenda separados por coma, devuelve 
     * otra cadena con las abraviaturas de los dias correspondientes, separados por guion
     * @param {string} diasAgenda Días en los que la agenda está disponible (0 = domingo, 6 = sábado)
     */
    getDiasAgendasAbv: function (diasAgenda) {
        var diasAgendaAbv = '';
        if (diasAgenda != '' && diasAgenda != null && diasAgenda != undefined) {
            var diasAgendaArray = diasAgenda.split(',');
            for (var i = 0; i < 7; i++) {
                var diaNro = parseInt(diasAgendaArray[i]);
                if (diasAgendaArray.indexOf(i.toString()) != -1) {
                    diasAgendaAbv += '<strong class="texto_pantalla">' + vars.diasSemana[i] + '</strong>';
                } else {
                    diasAgendaAbv += '<span class="texto_pantalla_gr">' + vars.diasSemana[i] + '</span>';
                }
                if (i < 6) {
                    diasAgendaAbv += '  -  ';
                }
            }
        }
        return diasAgendaAbv;
    }
}


var uConfig = {
    getConfig: function () {
        var configObject = {
            autoQueryStatus: window.localStorage.getItem('config__autoQueryStatus'),
            pacientQueryId: window.localStorage.getItem('config__pacientQueryId'),
            queryTimeFrame: window.localStorage.getItem('config__queryTimeFrame'),
            queryDaysUntilNext: window.localStorage.getItem('config__queryDaysUntilNext'),
            reproNotifications: window.localStorage.getItem('config__reproNotifications')
        };
        return configObject;
    },

    getConfigValue: function (key) {
        return window.localStorage.getItem(key);
    },

    save: function (key, value) {
        window.localStorage.setItem(key, value);
    },

    functions: {
        validatePatientId: function () {
            var id = parseInt($('#txtConfigIdPaciente').val());
            _loading.on('Validando...');
            _pacientes.getById(id, function (d) {
                _loading.off();
                navigator.notification.alert(uCadenas.getPacienteNombreCompleto(d), null, 'Paciente encontrado:', 'OK');
            }, function () {
                _loading.off();
                uMensajes.showError('Paciente no encontrado', 1, false);
            });
        },

        infoAutoQueryStatus: function () {
            navigator.notification.alert('La aplicación puede consultar los turnos reservados automáticamente '
                + 'para mostrarle a Ud. la encuesta antes de entrar al turno '
                + 'y la encuesta de satisfacción para que califique la atención recibida luego del turno.',
                null,
                'Consultar turnos próximos',
                'Entendido'
            );
        },

        infoPacientQueryId: function () {
            navigator.notification.alert(
                'Se puede especificar un paciente distinto al usuario registrado en la app. '
                + '\nEn cualquier caso, la aplicación consultará los turnos pedidos por un paciente (para otros) '
                + 'y los turnos para sí mismo, pudiendo ser el que se ingrese aquí o el que está registrado en la app.',
                null,
                'Paciente para consultar turnos',
                'Entendido'
            );
        },

        infoQueryTimeFrame: function () {
            navigator.notification.alert(
                'Especifique la frecuencia de tiempo (en minutos) con la que la aplicación consultará '
                + 'automáticamente los turnos.',
                null,
                'Tiempo para consultar turnos',
                'Entendido'
            );
        },

        infoReproNotifications: function () {
            navigator.notification.alert(
                'Cuando Ud. descarta una notificación, puede definir el tiempo en el que aparecerá de nuevo.\n'
                + 'Si se especifica "0", la notificación no se volverá a mostrar luego de que usted la descarte.',
                null,
                'Minutos para posponer notificaciones',
                'Entendido'
            );
        }
    }
}