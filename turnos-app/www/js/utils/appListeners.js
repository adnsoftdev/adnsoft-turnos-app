var listenersObject = {
    identificacionPacienteListener: function (elem) {
        if (vars.tipoFiltroBusquedaPaciente == 2 || vars.tipoFiltroBusquedaPaciente == 0) {
            // this.numeroListener(elem, 8);
            var valor = $(elem).val();
            if (valor.length > 8) {
                $(elem).val( $(elem).val().substr(0, 8) );
            } else {
                if (valor.indexOf('.') > -1 || valor.indexOf('-') > -1) {
                    uMensajes.showError('Indentificación incorrecta', 2, false);
                    $(elem).parent().addClass('valor_incorrecto');
                } else {
                    $(elem).parent().removeClass('valor_incorrecto');
                }
            }
        }
    },
    numeroListener: function (elem, maxLength) {
        var valor = $(elem).val();
        if (valor.length > maxLength) {
            $(elem).val( $(elem).val().substr(0, maxLength) );
        }
        if (this.numeroEntero(valor) == false) {
            // uMensajes.showError('Indentificación incorrecta', 2, false);
            // $(elem).parent().addClass('valor_incorrecto');
            // MOSTRAR ERROR
        }
    },
    numeroEntero: function (valor) {
        return ( Number.isInteger(valor) || parseInt(valor) == 0);
    },

    flipChangeListener: function () {
        $('#btnGuardarConfiguracion').removeClass('ui-disabled');
        var seleeccionado = $('#dActivarOpciones :checked').val();
        var configuracion = uConfig.getConfig();
        if (seleeccionado == 0) {
            $('#flip-1').val(0).slider('refresh');
            $('#dConfigIdPaciente').addClass('ui-disabled');
            $('#dConfigMinutos').addClass('ui-disabled');
            $('#dConfigDias').addClass('ui-disabled');
            $('#dConfigPosponer').addClass('ui-disabled');
        } else {
            $('#dConfigIdPaciente').removeClass('ui-disabled');
            $('#dConfigMinutos').removeClass('ui-disabled');
            $('#dConfigDias').removeClass('ui-disabled');
            $('#dConfigPosponer').removeClass('ui-disabled');
            if (configuracion.pacientQueryId != null && configuracion.queryDaysUntilNext != null && configuracion.queryTimeFrame != null) {
                $('#txtConfigIdPaciente').val(configuracion.pacientQueryId);
                $('#txtConfigMinutos').val(configuracion.queryTimeFrame);
                $('#txtConfigDias').val(configuracion.queryDaysUntilNext);
            }
        }   
    },

    inputDiaChangeListener: function (elem) {
        var valor = $(elem).val();
        if (valor != '') {
            if (valor.length > 2) {
                $(elem).val( valor.substr(0, 2) );
            }
            if (! uFecha.diaValido(parseInt(valor))) {
                uMensajes.showError('Valor incorrecto', 2, false);
                $(elem).parent().addClass('valor_incorrecto');
            } else {
                $(elem).parent().removeClass('valor_incorrecto');
            }
        }
    },
    inputMesChangeListener: function (elem) {
        var valor = $(elem).val();
        if (valor != '') {
            if (valor.length > 2) {
                $(elem).val( valor.substr(0, 2) );
            }
            if (! uFecha.mesValido(parseInt(valor))) {
                uMensajes.showError('Valor incorrecto', 2, false);
                $(elem).parent().addClass('valor_incorrecto');
            } else {
                $(elem).parent().removeClass('valor_incorrecto');
            }
        }
    },
    inputAnioChangeListener: function (elem) {
        var valor = $(elem).val();
        if (valor != '') {
            if (valor.length > 4) {
                $(elem).val( valor.substr(0, 4) );
            }
            if (! uFecha.anioValido(parseInt(valor))) {
                uMensajes.showError('Valor incorrecto', 2, false);
                $(elem).parent().addClass('valor_incorrecto');
            } else {
                $(elem).parent().removeClass('valor_incorrecto');
            }
        }
    },
    inputDiaMesFocusListener: function (elem) {
        if ($(elem).val() != '') {
            $(elem).val( parseInt($(elem).val()) );
        }
        $(elem).parent().removeClass('valor_incorrecto');
    },
    inputDiaBlurListener: function (elem) {
        var dia = $(elem).val();
        if (dia.length == 1) {
            $(elem).val('0' + dia);
        }
    },
    inputMesBlurListener: function (elem) {
        var mes = $(elem).val();
        if (mes.length == 1) {
            $(elem).val('0' + mes);
        }
    },
    inputFechaTotalListener: function (tableContainer) {
        var dia = $('#' + tableContainer + ' #dia').val();
        var mes = $('#' + tableContainer + ' #mes').val();
        var anio = $('#' + tableContainer + ' #anio').val();

        if (parseInt(mes) == 4 || parseInt(mes) == 6 || parseInt(mes) == 9 || parseInt(mes) == 11) {
            if (parseInt(dia) > 30) {
                $('#' + tableContainer + ' #dia').parent().addClass('valor_incorrecto');
                $('#' + tableContainer + ' #mes').parent().addClass('valor_incorrecto');
            }
        } else {
            if (parseInt(mes) == 2) {
                if (uFecha.anioBisiesto(parseInt(anio)) == true) {
                    if (parseInt(dia) > 29) {
                        $('#' + tableContainer + ' #dia').parent().addClass('valor_incorrecto');
                        $('#' + tableContainer + ' #mes').parent().addClass('valor_incorrecto');
                    } else {
                        $('#' + tableContainer + ' #dia').parent().removeClass('valor_incorrecto');
                        $('#' + tableContainer + ' #mes').parent().removeClass('valor_incorrecto');
                    }
                } else {
                    if (parseInt(dia) > 28) {
                        $('#' + tableContainer + ' #dia').parent().addClass('valor_incorrecto');
                        $('#' + tableContainer + ' #mes').parent().addClass('valor_incorrecto');
                    } else {
                        $('#' + tableContainer + ' #dia').parent().removeClass('valor_incorrecto');
                        $('#' + tableContainer + ' #mes').parent().removeClass('valor_incorrecto');
                    }
                }
            }
        }
    },

    rbOpcionesBusquedaListener: function () {
        var tipoBusqueda = $('#opcionesBusqueda :checked').val();
        $('#txtIdUsuarioConsulta').val('');
        if (parseInt(tipoBusqueda) == 1) {
            // $('#dIdUsuarioConsulta').addClass('div_oculto').hide();
            $('#dIdUsuarioConsulta').addClass('ui-disabled');
            $('#txtIdUsuarioConsulta').blur();
        } else {
            // $('#dIdUsuarioConsulta').removeClass('div_oculto').show();
            $('#dIdUsuarioConsulta').removeClass('ui-disabled');
            $('#txtIdUsuarioConsulta').focus();
        }
    },

    inputHoraChangeListener: function (elem) {
        var valor = $(elem).val();
        if (valor.length > 2) {
            $(elem).val( valor.substr(0, 2) );
            _vibrator.on(50);
        }
    }, 
    inputHoraBlurListener: function (elem) {
        var valor = $(elem).val();
        
        if (valor.length == 0) {
            $(elem).val('00');
            $(elem).parent().removeClass('valor_incorrecto');
        } else {
            if (this.numeroEntero(parseInt(valor))) {
                if (parseInt(valor) < 0 || parseInt(valor) > 23) {
                    uMensajes.showError('Valor incorrecto', 2, false);
                    $(elem).parent().addClass('valor_incorrecto');
                } else {
                    if (valor.length == 1) {
                        $(elem).val( '0' + valor );
                    }
                    $(elem).parent().removeClass('valor_incorrecto');
                }
            } else {
                uMensajes.showError('Valor incorrecto', 2, false);
                $(elem).parent().addClass('valor_incorrecto');
            }
        }
    },
    inputMinutoChangeListener: function (elem) {
        var valor = $(elem).val();
        if (valor.length > 2) {
            $(elem).val( valor.substr(0, 2) );
            _vibrator.on(50);
        }
    },
    inputMinutoBlurListener: function (elem) {
        var valor = $(elem).val();

        if (valor.length == 0) {
            $(elem).val('00');
            $(elem).parent().removeClass('valor_incorrecto');
        } else {
            if (this.numeroEntero(parseInt(valor))) {
                if (parseInt(valor) < 0 || parseInt(valor) > 59) {
                    uMensajes.showError('Valor incorrecto', 2, false);
                    $(elem).parent().addClass('valor_incorrecto');
                } else {
                    if (valor.length == 1) {
                        $(elem).val( '0' + valor );
                    }
                    $(elem).parent().removeClass('valor_incorrecto');
                }
            } else {
                uMensajes.showError('Valor incorrecto', 2, false);
                $(elem).parent().addClass('valor_incorrecto');
            }
        }
    }
}