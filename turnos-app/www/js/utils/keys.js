/**
 * Objeto contenedor de funciones relacionadas al botón 'back' del dispositivo
 */
var backbutton = {
    /**
     * Inicializa el comportamiento del botón back, dependiendo de la vista en la que se encuentre
     */
    init: function () {
        // _toast.show('back', '', '', 'bottom', 200);
        var vista = window.sessionStorage.getItem('_vista');
        switch (vista) {
            case 'index':
                // if ($('#dCondicionesApp').hasClass('ui-page-active')) {
                //     navigator.app.backHistory();
                // } else {
                    if (device.platform != 'iOS' && device.platform != 'browser') {
                        navigator.notification.confirm("¿Está seguro que desea salir?", function (opcion) {
                            if (opcion == 2) {
                                navigator.app.exitApp();
                            }
                        }, "Salir", "Cancelar, Aceptar");
                    }
                // }
                break;
            case 'menuPrincipal':
                if (device.platform != 'iOS' && device.platform != 'browser') {
                    navigator.notification.confirm("¿Está seguro que desea salir?", function (opcion) {
                        if (opcion == 2) {
                            navigator.app.exitApp();
                        }
                    }, "Salir", "Cancelar, Aceptar");
                }
                break;
            case 'registroNuevoPaciente':
                cancelarRegistroPaciente();
                break;
            case 'elegirTipoTurno':
                navMenuPrincipal('../', true, 'fade');
                break;
            case 'elegirCantPersonas':
                window.sessionStorage.removeItem('_tipoReserva');
                comenzarPedidoTurno('', true, 'slide');
                break;
            case 'ingresoIDPacientes':
                var orden = parseInt(window.sessionStorage.getItem('_ordenPacientes'));
                var totalPacientes = parseInt(window.sessionStorage.getItem('_cantidadPersonasTurno'));
                orden--;
                if (orden == 0) {
                    window.sessionStorage.removeItem('_cantidadPersonasTurno');
                    window.sessionStorage.removeItem('_ordenPacientes');
                    var tipoR = parseInt(window.sessionStorage.getItem('_tipoReserva'));
                    turnosOpcion2(tipoR, true, 'slide');
                } else {
                    $('#sOrdenPersonaCuerpo').text(orden);
                    $('#hProgresoFooter').text('Paciente ' + orden + ' de ' + totalPacientes);
                    window.sessionStorage.setItem('_ordenPacientes', orden);
                    if (orden == vars.usuariosTurno.grupoPacienteTitular.length) {
                        vars.usuariosTurno.grupoPacienteTitular.pop();
                    }
                    $('#dBotonesIDPacientes').hide();
                    $('#nombreIDPaciente').hide();
                    $('#txtIDPaciente').removeClass('ui-disabled');
                    $('#btnConfirmarIDPaciente').removeClass('ui-disabled');
                    $('#txtIDPaciente').val('');
                }
                break;
            case 'elegirPaciente':
                if ($('div.ui-popup-container').hasClass('ui-popup-active')) {
                    $('#popupAgendasPaciente').popup('close');
                    $('#popupAyudaPacientes').popup('close');
                } else {
                    navigator.notification.confirm('Si vuelve atrás, se perderá todo el progreso y se volverá al paso nº 1.\n'
                        + 'Tenga en cuenta que puede MODIFICAR o ELIMINAR un paciente desde esta pantalla', function (opcion) {
                            if (opcion == 1) {
                                vars.usuariosTurno.grupoPacienteTitular = [];
                                vars.pacientesTurno = [];
                                vars.pacientesEnviar.desde = null;
                                vars.pacientesEnviar.data = [];
                                comenzarPedidoTurno('', true, 'fade');
                            }
                        }, 'Atención', ['Descartar todos los cambios', 'Seguir aquí']);
                }
                break;
            case 'elegirQuincena':
                navElegirHorarios(true, 'slide');
                break;
            case 'elegirHorario':
            case 'elegirTipoAtencion':
                navElegirPaciente(null, true, 'pop');
                break;
            case 'buscadorAgenda':
                // if ( $('div.ui-popup-container').hasClass('ui-popup-active') ) {
                //     $('#popupEstudiosRadio').popup('close'); //popupEstudiosTel
                //     $('#popupEstudiosTel').popup('close');
                // } else {
                navElegirAtencion(vars.pacienteAgenda.pacienteId, vars.pacienteAgenda.pacienteNombre, true, 'slide');
                // }
                break;
            case 'calendario':
                cambiarQuincenaReserva();
                break;
            case 'confirmarReserva':
                // no hace nada
                break;
            case 'consultaTurnos':
                if ($('div.ui-popup-container').hasClass('ui-popup-active')) {
                    $('#popupInfoTurno').popup('close');
                } else {
                    navMenuPrincipal('../', true, 'fade');
                }
                break;
            case 'configuracion':
                navMenuPrincipal('', true, 'fade');
                break;
            case 'encuestaUsuario':
            case 'encuestaSatisfaccion':
                navigator.notification.confirm('Se cancelará la encuesta en curso.', function (n) {
                    if (n == 1) {
                        _toast.show('Encuesta cancelada', '', '', 'bottom', 'short');
                        navMenuPrincipal('', true, 'fade');
                    }
                }, 'Atención', ['Cancelar encuesta', 'Seguir aquí']);
                break;
            case 'estudiosRadiologicos':
                if ($('div.ui-popup-container').hasClass('ui-popup-active')) {
                    $('#popupEstudiosRadio').popup('close');
                    $('#popupEstudiosTel').popup('close');
                } else {
                    navBusquedaAgenda(2, 'slideup', true);
                }
                break;
            case 'condicionesApp':
                navUtil.to('', '#pIndex', 'pop', true, function () {
                    window.sessionStorage.setItem('_vista', 'index');
                });
                break;
            default:
                navigator.app.backHistory();
                break;
        }
    }
}

var enterKey = {
    press: function () {
        var vista = window.sessionStorage.getItem('_vista');
        switch (vista) {
            case 'buscadorAgenda':
                if (vars.busquedaTipoActual == 1) {
                    buscarAgenda();
                }
                if (vars.busquedaTipoActual == 2) {
                    buscarEspecialidad();
                }
                break;
            case 'consultaTurnos':
                obtenerTurnosReservados();
                $('#txtIdUsuarioConsulta').blur();
                break;
            default:
                break;
        }
    }
}