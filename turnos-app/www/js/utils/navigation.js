var navUtil = {
    /**
     * Navegación hacia otra página con una función callback para ejecutar luego de la carga
     * @param {string} dest Archivo html al que se quiere navegar
     * @param {string} page (opcional) ID del contenedor de la página que se va a mostrar
     * @param {string} tran Efecto de transición que se desea aplicar en la navegación
     * @param {string} r Indica si la transición debe ir **hacia atrás** (true) o **normal** (false)
     * @param {Function} cb (opcional) Función a ejecutar una vez que la página se carga
     */
    to: function (dest, page, tran, r, cb) {
        $(document).off('pagebeforeshow');
        $(document).on('pagebeforeshow', page, function () {
            if (cb != null || cb != undefined) {
                cb();
            }
        });
        $(':mobile-pagecontainer').pagecontainer('change', dest + (page != null ? (' ' + page) : ''), {
            // reload: true, 
            transition: tran,
            reverse: r,
            showLoadMsg: true
        });
    }
}