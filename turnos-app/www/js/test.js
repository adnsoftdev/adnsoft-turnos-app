var idnn = 25;

function test_encuestaUsuario() {
    idnn++;
    var idpacientetest = parseInt($('#idPacienteTest').val());
    window.localStorage.setItem('_abreEncuesta', 1);
    _notification.show(idnn, 'Turno con "ONETO CHRISTIAN"',
        'Debe realizar la siguiente encuesta antes del turno próximo a las HH:mm.', false,
        // [
        //     { id: 'yes', title: 'Comenzar' },
        //     { id: 'no', title: 'Rechazar' }
        // ], 
        null,
        { tipo: 'P', pacienteId: idpacientetest, pacienteNombre: 'PENNACHINI ERIC DANIEL', horaTurno: '14:00', agenda: 'RADIOLOGIA' }
    );
}

function test_encuestaDeSatisfaccion() {
    idnn++;
    var idpacientetest = parseInt($('#idPacienteTest').val());
    window.localStorage.setItem('_abreEncuesta', 1);
    _notification.show(idnn, 'Encuesta de satisfacción - "RADIOLOGÍA"',
        'Califique su atención', false,
        // [
        //     { id: 'yes', title: 'Comenzar' },
        //     { id: 'no', title: 'Rechazar' }
        // ], 
        null,
        { tipo: 'S', pacienteId: idpacientetest, pacienteNombre: 'PENNACHINI ERIC DANIEL', horaTurno: '14:00', agenda: 'RADIOLOGIA' }
    );
}