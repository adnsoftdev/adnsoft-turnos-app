/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        document.addEventListener('backbutton', this.onBackButton.bind(this), false);
        document.addEventListener('pause', this.onPause.bind(this), false);
    },

    onDeviceReady: function () {
        var plataforma = device.platform;

        _loading.on('Iniciando...');
        window.sessionStorage.setItem('_vista', 'index');

        // JQM: permitir transiciones a la misma página
        $.mobile.changePage.defaults.allowSamePageTransition = true;

        // 0080e0
        if (plataforma != 'browser') {
            StatusBar.backgroundColorByHexString("#f9f9f9");
            StatusBar.styleDefault();
        }
        if (plataforma == 'iOS') {
            navigator.splashscreen.hide();
            StatusBar.overlaysWebView(false);
        }

        // ocultar popup cuando se hace scroll vertical
        window.onscroll = function () {
            if ( $('div.ui-popup-container').hasClass('ui-popup-active') ) {
                $('#popupInfoTurno').popup('close');
            }
        };

        var login = window.localStorage.getItem('_login');
        if (login != null) {
            vars.usuarioLogueado = JSON.parse(login);
            _loading.off();
            navMenuPrincipal('views/', false, 'fade');
        } else {
            _loading.off();
            $('#pIndex').removeClass('div_oculto');
        }

        // configuracion de algunos eventos "pageshow"
        $(document).on('pageshow', '#pBuscadorAgenda', function () {
            $('#txtBusquedaAgenda').val('');
            $('#txtBusquedaAgenda').focus();
        });

        // cordova.plugins.backgroundMode.setDefaults({
        //     title: 'Clinica Parque se está ejecutando',
        //     text: 'Para mostrar las notificaciones, no cierre la aplicación',
        //     resume: true,
        //     hidden: false
        // });
        // cordova.plugins.backgroundMode.enable();

        /**
         * DISPARAR la consulta automática de turnos para mostrar la encuesta
         * una hora antes (o menos) del turno
         * Se hace cada 30 segundos (30.000 milisegundos)
         */
        var idNN = 0;
        var intervalConsultaTurno = setInterval(function () {
            if (window.localStorage.getItem('_login') != null) {
                mostrarNotificacionEncuestaPrevia();
                mostrarNotificacionEncuestaSatisfaccion();
            }
        }, 60000);

        // var intTest = setInterval(function () {
        //     idNN++;
        //     _notification.show(idNN, 'Prueba', 'Notificación de prueba', false, null, null);
        // }, 10000);

        // configuración de panel desplegable mediante el gesto 'swipe right'
        $(document).on('swiperight', '#pMenuPrincipal', function (e) {
            if ($(".ui-page-active").jqmData("panel") !== "open") {
                if (e.type === "swiperight") {
                    $("#panelOpciones").panel("open");
                }
            }
        });

        $(document).keypress(function(e) {
            if (e.which == 13) {
                enterKey.press();
            }
        });
        
        // setear los eventos click y clear de TODAS las notificaciones (globalmente)
        _notification.setOnClickListener(function (n, s) {
            // alert(JSON.stringify(s));
            var vistaActual = window.sessionStorage.getItem('_vista');
            if (vistaActual == 'encuestaUsuario' || vistaActual == 'encuestaSatisfaccion') {
                // alert('ya se está realizando una encuesta: ' + JSON.stringify(n));
                _toast.show('Ya se está realizando una encuesta', '', '', 'bottom', 1000);
                _notification.show(n.id, n.title, n.text, false, null, n.data);
            } else {
                switch (n.data.tipo) {
                    case 'P' :
                        _loading.on('Cargando...');
                        $(document).off('pagebeforeshow');
                        $(document).on('pagebeforeshow', '#pEncuestaUsuario', function () {
                            StatusBar.backgroundColorByHexString("#f9f9f9");
                            StatusBar.styleDefault();
                            window.sessionStorage.setItem('_vista', 'encuestaUsuario');
                            // seteo el id de turno, que es el mismo que el id de notificación
                            vars.encuestaEnviar.turnoId = n.id;
                            $('#pacienteEncuesta').text(n.data.pacienteNombre);
                            $('#agendaEncuesta').text(n.data.agenda);
                            $('#horarioTurnoEncuesta').text(n.data.horaTurno + ' hs.');
                        });
                        $(document).on('pageshow', '#pEncuestaUsuario', function () {
                            _loading.off();
                        });
                        $(':mobile-pagecontainer').pagecontainer('change', 'vEncuestaUsuario.html #pEncuestaUsuario', {
                            // reload: true, 
                            transition: 'face',
                            reverse: false, 
                            showLoadMsg: true
                        }); 
                        break;
                    case 'S':
                        _loading.on('Cargando...');
                        $(document).off('pagebeforeshow');
                        $(document).on('pagebeforeshow', '#pEncuestaSatisfaccion', function () {
                            $('.ui-slider-input').attr('readonly', 'readonly');
                            StatusBar.backgroundColorByHexString("#f9f9f9");
                            StatusBar.styleDefault();
                            window.sessionStorage.setItem('_vista', 'encuestaSatisfaccion');
                            // seteo el id de turno, que es el mismo que el id de notificación
                            vars.encuestaEnviar.turnoId = n.id;
                            $('#pacienteEncuesta').text(n.data.pacienteNombre);
                            $('#agendaEncuesta').text(n.data.agenda);
                            $('#horarioTurnoEncuesta').text(n.data.horaTurno + ' hs.');
                        });
                        $(document).on('pageshow', '#pEncuestaSatisfaccion', function () {
                            _loading.off();
                        });
                        $(':mobile-pagecontainer').pagecontainer('change', 'vEncuestaSatisfaccion.html #pEncuestaSatisfaccion', {
                            // reload: true, 
                            transition: 'face',
                            reverse: false,
                            showLoadMsg: true
                        });
                        break;
                    default:
                        break;
                }
            }
        });
        _notification.setOnClearListener(function (n) {
            _notification.showLater(n, 900000, function () {
                window.localStorage.setItem('_turno-' + n.data.tipo + '-repro-' + n.id, (new Date(Date.now() + 900000)));
                _toast.show('La notificación se mostrará de nuevo en 15 minutos', '', '', 'center', 'long');
            });
        });
    },

    onBackButton: function () {
        backbutton.init();
    },

    onHomeButton: function () {
        var vistaActual = window.sessionStorage.getItem('_vista');
        if (vistaActual != 'consultaTurnos') {
            navigator.notification.confirm('Si vuelve al menú principal, se perderán todos los cambios.', function (num) {
                if (num == 1) {
                    cancelarReservaCompleta();
                }
            }, '', ['Ir al menú principal', 'Cancelar']);
        } else {
            vars.reiniciarDatos();
            navMenuPrincipal('../', true, 'slideup');
        }
    },

    onPause: function () {
        window.localStorage.setItem('_ultimoAcceso', uFecha.getFechaString(new Date()));
    },

    onChangeOrientation: function (e) {
        var vista = window.sessionStorage.getItem('_vista');
        switch (vista) {
            case 'elegirPaciente':
                var anchoPadre = $('#dPacientesCargados .ui-controlgroup-controls').css('width');
                console.debug('>>>>>>>>>>>>>>>> Cambio a modo ' + e.orientation);
                console.log('Ancho visual: ' + anchoPadre);
                $('#dPacientesCargados .paciente').css('width', ( (parseInt(anchoPadre) / 2) + (parseInt(anchoPadre) / 4) - 32) ); // 50%
                console.log('Ancho elem. izquierdo: ' + ((parseInt(anchoPadre) / 2) + (parseInt(anchoPadre) / 4) - 32) );
                $('#dPacientesCargados .opcion').css('width', ((parseInt(anchoPadre) / 8) - 32) ); // 25%
                console.log('Ancho elem. derecho: ' + ((parseInt(anchoPadre) / 8) - 32) );
                break;
            default:
                break;
        }
        
    }
};

app.initialize();

// function testNot() {
//     cordova.plugins.notification.local.getAll(function (notifications) {
//         var notif = null;
//         for (var i = 0; i < notifications.length; i++) {
//             if (notifications[i].text == 'Aplicación iniciada') {
//                 notif = notifications[i];
//                 break;
//             }
//         }
//         if (notif != null) {
//             cordova.plugins.notification.local.update({
//                 id: notif.id,
//                 text: notif.text,
//                 badge: (notif.badge == 0 ? 2 : (notif.badge + 1))
//             });
//         } else {
//             _notification.show('Aplicación iniciada');
//         }
//     });
// }