function navRegistroNuevoPaciente() {
    navUtil.to('registroNuevoPaciente/vRegistroNuevoPaciente.html', null, 'pop', false, function () {
        window.sessionStorage.setItem('_vista', 'registroNuevoPaciente');
    });
}

function confirmarRegistroPaciente() {
    // 1) leer campos (convertir formatos)
    // 2) guardar objeto paciente en localStorage (tiene que ser 'local' asi se puede levantar
    //    al abrir la aplicación en caso de que anteriormente no se haya podido enviar al ws)
    // 3) enviar datos ingresados al web-service
    // 3.1) si se envió satisfactoriamente, borrar el objeto de localStorage
    var nuevoPaciente = {
        apellido: $('#txtApellidoPac').val(),
        nombre: $('#txtNombrePac').val(),
        fechaNacimiento: $('#dtpFechaNacimientoPac').val(),
        telefono: $('#txtTelefonoPac').val(),
        email: $('#txtEmailPac').val(),
        comentario: $('#tarComentarioPac').val()
    }
    // window.localStorage.setItem('_pacienteNuevo', JSON.stringify(nuevoPaciente));

    /*
    ws.call( ... )
    */
    _toast.show('Paciente registrado con éxito', '#0080e0', '', 'center', 'long');
    navMenuPrincipal('..', true);
}

function cancelarRegistroPaciente() {
    if ( $('#txtApellidoPac').val() != '' ||
        $('#txtNombrePac').val() != '' ||
        $('#dtpFechaNacimientoPac').val() != '' ||
        $('#txtTelefonoPac').val() != '' ||
        $('#txtEmailPac').val() != '' ||
        $('#tarComentarioPac').val() != '' ) {
        navigator.notification.confirm('Si vuelve se perderán los cambios realizados.', function (n) {
            if (n == 2) {
                navMenuPrincipal('..', true);
            }
        }, 'Atención', ['Seguir aquí', 'Descartar cambios']);
    } else {
        window.sessionStorage.setItem('_vista', 'menuPrincipal');
        navMenuPrincipal('..');
    }
}