function navConsultaTurnos(mod, r) {
    navUtil.to(mod + '/vElegirTipoConsulta.html', '#pElegirTipoConsulta', 'fade', r, function () {
        window.sessionStorage.setItem('_vista', 'consultaTurnos');
        $('#dNombrePacienteInput').hide();
    });
}

function obtenerTurnosReservados(par) {
    var tipoBusqueda = par;
    var idPaciente = vars.usuarioLogueado.id;
    vars.tipoConsultaTurnos = parseInt(tipoBusqueda);
    _loading.on('Buscando...');

    _turnos.getTurnosReservadosPorModo(vars.tipoConsultaTurnos, idPaciente, function (data) {
        $('#ulResultadosBusqueda').empty();
        vars.turnosConsultados = [];
        for (var i = 0; i < data.length; i++) {
            if (vars.tipoConsultaTurnos == 1 || (vars.tipoConsultaTurnos == 2 && vars.usuarioLogueado.id != data[i].pacienteId)) {
                vars.turnosConsultados.push(data[i]);
            }
        }
        if (vars.turnosConsultados.length > 0) {
            $('#ulResultadosBusqueda').addClass('ui-shadow');
            for (var i = 0; i < vars.turnosConsultados.length; i++) {
                var turno = vars.turnosConsultados[i];
                // vars.turnosConsultados.push(turno);
                if (vars.tipoConsultaTurnos == 1 || (vars.tipoConsultaTurnos == 2 && vars.usuarioLogueado.id != turno.pacienteId)) {
                    $('#ulResultadosBusqueda').append('<li>' +
                        '<a onclick="verDetalleTurno(' + turno.turnoId + ')">' +
                        '<h1>' + uFecha.getFechaFormatoNormal(turno.fecha.split('T')[0]) + ' - ' +
                        uFecha.getHoraCorta(turno.hora) + '</h1>' +
                        '<p>' + turno.agenda + (turno.observaciones_turno == '' ? '' : (' <span class="texto_pantalla_gr">(' + turno.observaciones_turno) + ')</span>') + '</p>' +
                        (vars.tipoConsultaTurnos == 2 
                            ? ('<p class="texto_pantalla">Paciente: <strong>' + turno.paciente + '</strong></p>') : '') +
                        '</a>' +
                        '</li>'
                    );
                }
            }
            $('#ulResultadosBusqueda').listview('refresh');
        } else {
            $('#ulResultadosBusqueda').removeClass('ui-shadow');
            $('#ulResultadosBusqueda').append('<li class="center_text"><code>Sin resultados</code></li>');
            $('#ulResultadosBusqueda').listview('refresh');
        }
        _loading.off();
    }, function (j) {
        _loading.off();
        uMensajes.showError('No se pudo obtener los datos de los turnos', 2, false);
        var msg = (j.responseJSON == undefined ? 'Sin conexión' : 'Error');
        $('#ulResultadosBusqueda').removeClass('ui-shadow');
        $('#ulResultadosBusqueda').append('<li class="center_text"><code>' + msg + '</code></li>');
        $('#ulResultadosBusqueda').listview('refresh');
    });
}

/**
 * Busca un turno por su ID en la variable array
 * @param {number} idTurno Id de turno
 */
function verDetalleTurno(idTurno) {
    var turnos = vars.turnosConsultados;
    var turno = null;
    for (var i = 0; i < turnos.length; i++) {
        if (turnos[i].turnoId == idTurno) {
            turno = turnos[i];
            break;
        }
    }
    if (turno != null) {
        $('#h1FechaHoraTurno').text(
            uFecha.getFechaFormatoNormal(turno.fecha.split('T')[0]) + ' - ' + 
            uFecha.getHoraCorta(turno.hora)
        );
        $('#hdTurnoId').val(idTurno);
        $('#sAgendaTurnoConsulta').html(turno.agenda + (turno.observaciones_turno == '' ? '' : (' <span class="texto_pantalla_gr">(' + turno.observaciones_turno + ')</span>')));
        var fechaR = uFecha.getFechaFormatoNormal( turno.fecha_reserva.split('T')[0] );
        var horaR = uFecha.getHoraCorta( turno.fecha_reserva.split('T')[1].split('.')[0] );
        $('#sFechaReservaConsulta').text(fechaR);
        $('#sHoraReservaConsulta').text(horaR);
        if (vars.tipoConsultaTurnos == 2) {
            $('#dPacientePopup').show();
            $('#sPacientePopup').text(turno.paciente.trim());
        } else {
            $('#dPacientePopup').hide();
        }
        $('#popupInfoTurno').popup('open');
    }
}

/**
 * Envía la cancelación de un turno. El ID de turno lo obtiene del input hidden
 */
function cancelarTurno() {
    var idTurno = $('#hdTurnoId').val();
    var personaId = vars.usuarioLogueado.id;
    navigator.notification.confirm('Está seguro de cancelar el turno?', function (n) {
        if (n == 1) {
            _loading.on('Cancelando...');
            _turnos.cancelarTurno(idTurno, personaId, function (d) {
                _loading.off();
                _toast.show('Turno cancelado', '#0080e0', '', 'bottom', 1000);
                $('#popupInfoTurno').popup('close');
                obtenerTurnosReservados(vars.tipoConsultaTurnos);
            }, function () {
                _loading.off();
                uMensajes.showError('No se ha podido cancelar el turno.\nIntente de nuevo más tarde', 1, false);
            });
        }
    }, 'Confirmar', ['Si', 'No']);
}

