var _turnos = {
    /**
     * Envía la petición al web-service para obtener los turnos libres de un grupo de agendas en un intervalo de fecha.
     * @param {Date} desde Fecha de inicio
     * @param {Date} hasta Fecha de fin
     * @param {string} agendas IDs de agendas separados por coma
     * @param {Function} success Función de éxito
     */
    getTurnosLibres: function (desde, hasta, agendas, success) {
        var envio = { desde: '', hasta: '', agendas: '' };
        envio.desde = uFecha.getFechaStringDBsinHora(desde);
        envio.hasta = uFecha.getFechaStringDBsinHora(hasta);
        envio.agendas = agendas;
        ws.call(vars.url + '/disponibilidad-turnos-libres', 'POST', envio, function (e, st, tx) {
            _loading.off();
            if (st == 'timeout') {
                uMensajes.showError('Se agotó el tiempo de espera', 1, false);
            } else {
                uMensajes.showError(e.responseJSON, 1, false);
            }
        }, function (d) {
            success(d);
        });
    },

    /**
     * Obtiene el menor horario disponible para una agenda en determinada fecha
     * @param {Number} idAgenda 
     * @param {Date} fecha 
     * @param {Array} arrLibres 
     */
    getMinimoHorarioDia: function (idAgenda, fecha, arrLibres) {
        for (var i = 0; i < arrLibres.length; i++) {
            if (uFecha.mismaFecha(fecha, arrLibres[i].fechaHora) && arrLibres[i].agenda == idAgenda) {
                return arrLibres[i];
            }
        }
        return {
            agenda: idAgenda,
            agendaNombre: _agendas.getById(idAgenda).nombre,
            agendaTipo: _agendas.getById(idAgenda).tipoId,
            fecha: null,
            hora: null,
            fechaHora: new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate(), 0, 0, 0),
            horarioId: null,
            franja: null,
            ocupado: false
        };
    },

    /**
     * Devuelve los turnos reservados para un paciente
     * @param {number} modo Tipo de busqueda *(1: **paciente titular**, 2: **otros**)*
     * @param {number} idPaciente ID de paciente en cuestión
     * @param {Function} success Función de éxito, con el array de turnos encontrados
     * @param {Function} fail Función de error
     */
    getTurnosReservadosPorModo: function (modo, idPaciente, success, fail) {
        var url = '';
        switch (modo) {
            case 1:
                url = vars.url + '/turnos-paciente/' + idPaciente;
                break;
            case 2:
                url = vars.url + '/turnos-reservado-por/' + idPaciente;
                break;
            default:
                break;
        }
        ws.call(url, 'GET', null, function (j) {
            if (fail != null && fail != undefined) {
                fail(j);
            }
        }, function (d) { 
            success(d);
        });
    },

    getTurnosReservados: function (success) {
        var idPacienteRef = vars.usuarioLogueado.id;
        var turnosSiguientes = [];
        /* INSERTAR turnoId,pacienteNombre,fechaHora,agenda */
        _turnos.getTurnosReservadosPorModo(1, idPacienteRef, function (data1) {
            for (var i = 0; i < data1.length; i++) {
                turnosSiguientes.push({
                    turnoId: data1[i].turnoId,
                    pacienteId: idPacienteRef,
                    pacienteNombre: uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado),
                    fechaHora: uFecha.getDateFromStringDb(data1[i].fecha.split('T')[0] + ' ' + data1[i].hora),
                    agenda: data1[i].agenda
                });
            }
            _turnos.getTurnosReservadosPorModo(2, idPacienteRef, function (data2) {
                for (var i = 0; i < data2.length; i++) {
                    turnosSiguientes.push({
                        turnoId: data2[i].turnoId,
                        pacienteId: data2[i].pacienteId,
                        pacienteNombre: data2[i].paciente.trim(),
                        fechaHora: uFecha.getDateFromStringDb(data2[i].fecha.split('T')[0] + ' ' + data2[i].hora),
                        agenda: data2[i].agenda
                    });
                }
                // ordenar turnos por horario
                turnosSiguientes.sort(function (a, b) {
                    return a.fechaHora - b.fechaHora;
                });
                
                success(turnosSiguientes);
            }, null);
        }, null);
    },

    cancelarTurno: function (idTurno, idPaciente, success, fail) {
        ws.call(vars.url + '/cancela-turno-app/' + idTurno + '/' + idPaciente, 'POST', {}, function (e1, e2, e3) {
            fail();
        }, function (d) {
            success(d);
        });
    },

    enviarEncuestaPrevia: function (idTurno, data, success, error) {
        ws.call(vars.url + '/reconfirmacion-turno/' + idTurno, 'POST', data, function (j, s, e) {
            error(j);
        }, function (d) {
            success(d);
        });
    },

    enviarEncuestaSatisfaccion: function (idTurno, calificacion, success, fail) {
        ws.call(vars.url + '/califica-turno/' + idTurno + '/' + calificacion, 'POST', {}, function (j, s, e) {
            fail(j);
        }, function (d) {
            success(d);
        });
    }
}