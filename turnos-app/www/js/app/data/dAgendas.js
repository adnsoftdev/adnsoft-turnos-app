var _agendas = {
    getById: function (id) {
        var objAgendas = JSON.parse(window.localStorage.getItem('_agendas'));
        var agendaEncontrada = null;
        for (var i = 0; i < objAgendas.length; i++) {
            if (objAgendas[i].agendaId == id) {
                agendaEncontrada = objAgendas[i];
                break;
            }
        }
        return agendaEncontrada;
    },

    /**
     * Lista todas las agendas disponibles, enviando la solicitud al webservice
     * @param {Function} [success] Trae los datos de las agendas
     * @param {Function} [fail] Error de la solicitud
     */
    list: function (success, fail) {
        ws.call(vars.url + '/agendas', 'GET', null, function (j, text, error) {
            if (fail != null && fail != undefined) {
                fail();
            }
        }, function (d) {
            window.localStorage.setItem('_agendas', JSON.stringify(d));
            // window.localStorage.setItem('_agendas_act', uFecha.getFechaString(new Date()));
            if (success != null && success != undefined) {
                success(d);
            }
        });
    },

    /**
     * Valida si hay o no coincidencias de días para todas las agendas y pacientes elegidos.
     * @param {Function} success Función a ejecutar si hay coincidencias de por lo menos un día para todas las agendas
     * @param {Function} fail Función a ejecutar si no hay ninguna coincidencia de días
     */
    validarDiasCoincidentes: function (success, fail) {
        var diasCoincidentes = 0;
        for (var i = 0; i < 7; i++) {
            var contadorPorDia = 0;
            for (var j = 0; j < vars.pacientesTurno.length; j++) {
                if (vars.pacientesTurno[j].agendaDias.indexOf(i.toString()) != -1) {
                    contadorPorDia++;
                }
            }
            if (contadorPorDia == vars.pacientesTurno.length) {
                diasCoincidentes++;
            }
        }
        if (diasCoincidentes > 0) {
            success();
        } else {
            fail();
        }
    }
}