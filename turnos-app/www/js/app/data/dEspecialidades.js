var _especialidades = {
    getById: function (id, success) {
        this.list(function (d) {
            for (var i = 0; i < d.length; i++) {
                if (id == d[i].id) {
                    success(d[i]);
                    break;
                }
            }
        }, null);
    },

    /**
     * Lista las especialidades haciendo la petición al web-service
     * @param {Function} [success] 
     * @param {Function} [fail] 
     */
    list: function (success, fail) {
        ws.call(vars.url + '/especialidades', 'GET', null, function (j, text, error) {
            if (fail != null && fail != undefined) {
                fail();
            }
        }, function (d) {
            success(d);
        });
    },

    /**
     * 
     * @param {number} idEspecialidad Id de especialidad a buscar sus agendas
     * @param {Function} success Función de éxito
     * @param {Function} [fail] Función de error
     */
    getAgendas: function (idEspecialidad, success, fail) {
        ws.call(vars.url + '/agendas-especialidad/' + idEspecialidad, 'GET', null, function (j, text, error) {
            if (fail != null && fail != undefined) {
                fail();
            }
        }, function (d) {
            var agendasArray = [];
            for (var i = 0; i < d.length; i++) {
                var agenda = _agendas.getById(d[i].agendaId);
                if (agenda != null) {
                    agendasArray.push(agenda);
                }
            }
            success(agendasArray);
        });
    }
}