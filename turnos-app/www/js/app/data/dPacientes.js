var _pacientes = {
    getById: function (id, success, fail) {
        ws.call(vars.url + '/get-persona-id/' + id, 'GET', null, function (j, text, error) {
            if (fail != null && fail != undefined) {
                fail();
            }
        }, function (d) {
            success(d);
        });
    },

    getByIdFecha: function (id, fecha, success, fail) {
        var send = { personaId: id, fechaNacimiento: fecha };
        ws.call(vars.url + '/get-persona-id-fecha', 'POST', send, function (j, text, error) {
            fail(j);
        }, function (data) {
            success(data);
        });
    },

    getByDocumentoFecha: function (documento, fecha, success, fail) {
        var send = { numero: documento, fechaNacimiento: fecha };
        ws.call(vars.url + '/get-persona-documento-fecha', 'POST', send, function (j, text, error) {
            fail(j);
        }, function (data) {
            success(data);
        });
    },

    /**
     * Realiza la verificación de la persona al momento de registrarla en la app
     * @param {any} data Objeto de datos que se necesitan para verificar la persona
     * @param {Function} success Función callback de éxito (si la respuesta es **true**)
     * @param {Function} fail Función de error (problema de conexión o si no se encontró la persona)
     */
    check: function (data, success, fail) {
        ws.call(vars.url + '/check-persona', 'POST', data, function (j1, t1, e1) {
            if (j1.responseJSON == 'Persona no encontrada!') {
                _loading.off();
                navigator.notification.alert('Verifique los datos ingresados y reintente de nuevo.', null, 'Persona no encontrada', 'OK');
            } else {
                if (fail != null && fail != undefined) {
                    fail();
                }
            }
        }, function (datos) {
            success(datos);
        });
    }
}