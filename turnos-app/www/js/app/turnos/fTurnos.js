/////////////////////////////////////////////////////////////////////////////////
/*
Manejo general de turnos
> Funciones para el manejo de turnos, especialmente el inicio del proceso y otras funciones generales.
*/
////////////////////////////////////////////////////////////////////////////////

/**
 * Muestra errores genereales no manejados
 */
window.onerror = function (errMsg, url, lineNumber) {
    // window.plugins.toast.showLongTop('Error general en la aplicación: ' + errMsg + '\n' + url + '\n' + lineNumber);
    alert('Error general en la aplicación: ' + errMsg + '\n' + url + '\n' + lineNumber);
}

function btnMenuClick() {
    app.onHomeButton();
}

function iosHeader() {
    // probar acá
    var header = $('#btnVolverIOS');
    if (header != null && header != undefined) {
        if (device.platform != 'iOS') {
            $('#btnVolverIOS').hide();
        }
    }
}

function navCondicionesApp() {
    navUtil.to('', '#dCondicionesApp', 'pop', false, function () {
        window.sessionStorage.setItem('_vista', 'condicionesApp');
        var anioActual = (new Date()).getFullYear();
        // alert(anioActual);
        $('#sCondicionesAnioActual').text(anioActual);
    });
}


/**
 * Maneja el ingreso por primera vez del usuario, validando con ID de usuario
 */
function ingresoUsuario(e) {
    if (e != undefined && e != null) {
        e.preventDefault();
    }
    var dia = $('#tFechaIngresoUsuario #dia').val();
    var mes = $('#tFechaIngresoUsuario #mes').val();
    var anio = $('#tFechaIngresoUsuario #anio').val();

    if ($('#txtIdUsuarioIngreso').val() == '' ||
        $('#txtDNIIngreso').val() == '' ||
        dia == '' || mes == '' || anio == '' ||
        $('#txtApellidoMat').val() == '' ||
        $('#txtTelefono').val() == '') {
        _vibrator.on([50, 100, 50, 100]);
        navigator.notification.alert('Complete todos los campos antes de continuar', null, '', 'Entendido');
        return;
    } else {
        if (moment(anio + '-' + mes + '-' + dia).isValid() == false) {
            uMensajes.showError('Fecha incorrecta', 2, false);
            listenersObject.inputFechaTotalListener('tFechaIngresoUsuario');
            return;
        }
    }
    var dataEnviar = {
        personaId: $('#txtIdUsuarioIngreso').val(),
        documento: $('#txtDNIIngreso').val(),
        nacimiento: ( $('#anio').val() + '-' + $('#mes').val() + '-' + $('#dia').val() ),
        apellidoMaterno: $('#txtApellidoMat').val().trim()
    }; 
    _loading.on('Comprobando usuario...');
    _pacientes.check(dataEnviar, function (datos) {
        if (datos == true) {
            _loading.off();
            _loading.on('Obteniendo datos de usuario...');
            _pacientes.getById(dataEnviar.personaId, function (d) {
                _loading.off();
                vars.usuarioLogueado = d;
                window.localStorage.setItem('_login', JSON.stringify(d));
                navMenuPrincipal('views/', false);
            }, function () {
                _loading.off();
                navigator.notification.alert('No se pudo obtener los datos del usuario.\nReintente de nuevo más tarde.', null, 'Error', 'OK');
            });
        }
    }, function () {
        navigator.notification.alert('No se pudo comprobar el usuario.\nReintente de nuevo más tarde.', null, 'Error', 'OK');
    });
    return false;
}

function desvincularUsuario() {
    var msg = (
        device.platform == 'iOS'
            ? 'Si continúa, se borrarán los datos del usuario y se mostrarán los campos para ingresar uno nuevo.'
            : 'Si continúa, la aplicación se cerrará y se borrarán los datos del usuario.'
    );
    navigator.notification.confirm(msg, function (s) {
        if (s == 1) {
            window.localStorage.clear();
            window.sessionStorage.clear();
            _notification.clearAll();
            // Si es Android, se cierra la app
            // Si es iOS, no se puede cerrar entonces se redirige a la vista de registro
            if (device.platform != 'iOS' && device.platform != 'browser') {
                navigator.app.exitApp();
            } else {
                navUtil.to('../index.html', null, 'fade', false, function () {
                    StatusBar.backgroundColorByHexString("#f9f9f9");
                    StatusBar.styleDefault();
                    $('#pIndex').removeClass('div_oculto');
                    window.sessionStorage.setItem('_vista', 'index');
                    $('#txtIdUsuarioIngreso').val('');
                    $('#txtDNIIngreso').val('');
                    $('#txtFechaNac').val('');
                    $('#txtApellidoMat').val('');
                });
            }
        }
    }, 'Desvincular usuario actual', ['Aceptar', 'Cancelar']);
}

function navMenuPrincipal(mod, r, t) {
    // configurar panel izquierdo (version app)
    _appVersion.get(function (v) {
        navUtil.to(mod + 'vMenuPrincipal.html', null, (t != undefined ? t : 'fade'), r, function () {
            StatusBar.backgroundColorByHexString("#0080e0");
            StatusBar.styleLightContent();
            window.sessionStorage.setItem('_vista', 'menuPrincipal');
            $('#h4BienvenidaUsuario').text('Bienvenido: ' + uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado));
            $('#pInfoVersionApp').text(v);
            var ultimoAcceso = window.localStorage.getItem('_ultimoAcceso');
            $('#pFechaHoraUltAcceso').text(ultimoAcceso == null ? '-' : ultimoAcceso);
        });
    });
}

/**
 * Navega a la primer vista del proceso de turno
 */
function comenzarPedidoTurno(mod, r, t) {
    navUtil.to(mod + 'vElegirTipoTurno.html', null, t, r, function () {
        iosHeader();
        window.sessionStorage.setItem('_vista', 'elegirTipoTurno');
    });
}

/**
 * Turno de titular solo
 */
function turnosIndividual(r) {
    window.sessionStorage.setItem('_tipoReserva', 0);
    // vars.pacienteAgenda.pacienteId = vars.usuarioLogueado.id;
    navElegirPaciente(checkPrimerUso, r, 'fade');
}

/**
 * Maneja el caso del tipo de reserva con pacientes además del titular
 * @param {number} par Tipo de reserva (salvando la individual): 1 es titular más pacientes; 2 es solo pacientes
 */
function turnosOpcion2(par, r, t) {
    window.sessionStorage.setItem('_tipoReserva', par);
    navUtil.to('vElegirCantPersonas.html', '#pElegirCantPersonas', t, r, function () {
        window.sessionStorage.setItem('_vista', 'elegirCantPersonas');
        switch (par) {
            case 1:
                $('#dCantPersonasA').show();
                $('#dCantPersonasB').hide();
                $('#h2EleccionModoTurno').text('Elija la cantidad de personas que se atenderán (además de usted)');
                break;
            case 2:
                $('#dCantPersonasA').hide();
                $('#dCantPersonasB').show();
                $('#h2EleccionModoTurno').text('Elija la cantidad total de personas que se atenderán');
                break;
            default:
                break;
        }
    });
}


/**
 * Navega hacia la vista de configuración
 */
function navConfiguracion() {
    navUtil.to('vConfiguracion.html', '#pConfiguracion', 'fade', false, function () {
        window.sessionStorage.setItem('_vista', 'configuracion');
        StatusBar.backgroundColorByHexString("#1d1d1d");
        $('#btnGuardarConfiguracion').removeClass('ui-shadow').addClass('ui-disabled')
        // debería tomarlo de algun lugar almacenado
        var configuracion = uConfig.getConfig();
        if (configuracion.autoQueryStatus == 0 || configuracion.autoQueryStatus == null) {
            $('#flip-1').val(0).slider('refresh');
            $('#dConfigIdPaciente').addClass('ui-disabled');
            $('#dConfigMinutos').addClass('ui-disabled');
            $('#dConfigDias').addClass('ui-disabled');
            $('#dConfigPosponer').addClass('ui-disabled');
        } else {
            $('#flip-1').val(1).slider('refresh');
            $('#dConfigIdPaciente').removeClass('ui-disabled');
            $('#dConfigMinutos').removeClass('ui-disabled');
            $('#dConfigDias').removeClass('ui-disabled');
            $('#dConfigPosponer').removeClass('ui-disabled');
            $('#txtConfigIdPaciente').val(configuracion.pacientQueryId);
            $('#txtConfigMinutos').val(configuracion.queryTimeFrame);
            $('#txtConfigDias').val(configuracion.queryDaysUntilNext);
        }        
    });
}

function guardarConfiguracion() {
    $('#btnGuardarConfiguracion').addClass('ui-disabled');
    var configActual = uConfig.getConfig();
    // reemplazo de valores
    configActual.autoQueryStatus = $('#dActivarOpciones :checked').val();
    configActual.pacientQueryId = $('#txtConfigIdPaciente').val();
    configActual.queryDaysUntilNext = $('#txtConfigDias').val();
    configActual.queryTimeFrame = $('#txtConfigMinutos').val();
    uConfig.save('config__autoQueryStatus', configActual.autoQueryStatus);
    uConfig.save('config__pacientQueryId', configActual.pacientQueryId);
    uConfig.save('config__queryDaysUntilNext', configActual.queryDaysUntilNext);
    uConfig.save('config__queryTimeFrame', configActual.queryTimeFrame);
    _toast.show('Configuración guardada', '', '', 'bottom', 'short');
}


function mostrarNotificacionEncuestaPrevia() {
    _turnos.getTurnosReservados(function (d) {
        if (d.length > 0) {
            for (var i = 0; i < d.length; i++) {
                var encuestaMostrada = window.localStorage.getItem('_turno-' + d[i].turnoId);
                if (encuestaMostrada == null || parseInt(encuestaMostrada) == 0) {
                    var fechaTurno = d[i].fechaHora;
                    var fechaActual = new Date();
                    var diferencia = fechaTurno.getTime() - fechaActual.getTime();
                    if (diferencia <= 3600000 && diferencia > 0) {
                        var idt = d[i].turnoId;
                        var idp = d[i].pacienteId;
                        var pac = d[i].pacienteNombre;
                        var agn = d[i].agenda;
                        // var htr = uFecha.getHoraCorta(d[i].hora);
                        var htr = uFecha.getFechaStringHoraCorta(d[i].fechaHora);
                        window.localStorage.setItem('_abreEncuesta', 1);
                        _notification.show(
                            idt, 
                            'Encuesta para "' + d[i].pacienteNombre + '"',
                            'Turno: ' + d[i].agenda + '\n' + uFecha.getFechaStringHoraCorta(d[i].fechaHora),
                            false, 
                            // [
                            //     { id: 'yes', title: 'Comenzar' },
                            //     { id: 'no', title: 'Rechazar' }
                            // ], 
                            null,
                            {tipo: 'P', pacienteId: idp, pacienteNombre: pac, horaTurno: htr, agenda: agn}
                        );
                        window.localStorage.setItem('_turno-' + idt, 1);
                    }
                }
            }
        }
    }); 
}

function mostrarNotificacionEncuestaSatisfaccion() {
    _turnos.getTurnosReservados(function (d) {
        if (d.length > 0) {
            for (var i = 0; i < d.length; i++) {
                var encuestaMostrada = window.localStorage.getItem('_turno-s-' + d[i].turnoId);
                if (encuestaMostrada == null || parseInt(encuestaMostrada) == 0) {
                    var fechaTurno = d[i].fechaHora;
                    var fechaActual = new Date();
                    // fecha actual menos la fecha del turno (si es positivo el turno ya pasó)
                    var diferencia = fechaActual.getTime() - fechaTurno.getTime();
                    if (diferencia >= 3600000) {
                        var idt = d[i].turnoId;
                        var idp = d[i].pacienteId;
                        var pac = d[i].pacienteNombre;
                        var agn = d[i].agenda;
                        var htr = uFecha.getFechaStringHoraCorta(d[i].fechaHora);
                        window.localStorage.setItem('_abreEncuesta', 1);
                        _notification.show(
                            idt, 
                            'Calificación de turno',
                            uFecha.getFechaStringHoraCorta(d[i].fechaHora) + ' - Agenda: ' + agn
                                + '\nHaga click aquí para realizar la calificación',
                            false, null, {
                                tipo: 'S',
                                pacienteId: idp, 
                                pacienteNombre: pac, 
                                horaTurno: htr, 
                                agenda: agn
                            }
                        );
                        window.localStorage.setItem('_turno-s-' + idt, 1);
                    }
                }
            }
        }
    }); 
}

function enviarEncuestaPreviaTurno() {
    _loading.on('Enviando...');
    var contMemoMotivoConsulta = $('#memoMotivoConsulta').val().trim();
    var contMemoRemedios = $('#memoRemedios').val().trim();
    // var selNecesitaCertificado = $('#memoNecesitaCertificado :checked').val();
    // var textoNecesitaCertificado = (selNecesitaCertificado == 1 ? 'SI' : 'NO');
    _turnos.enviarEncuestaPrevia(vars.encuestaEnviar.turnoId, {
        motivoConsulta: contMemoMotivoConsulta,
        medicacion: contMemoRemedios
    }, function (d) {
        if (d == true) {
            _loading.off();
            _toast.show('Encuesta enviada', '', '', 'bottom', 3000);
            navMenuPrincipal('', true, 'fade');
        }
    }, function (j) {
        _loading.off();
        uMensajes.showError('Error al enviar encuesta', 1, false);
    });
}

function enviarEncuestaSatisfaccion() {
    _loading.on('Enviando...');
    var calificacionElegida = parseInt($('#sldCalificacionTurno').val());
    _turnos.enviarEncuestaSatisfaccion(vars.encuestaEnviar.turnoId, calificacionElegida, function (d) {
        if (d == true) {
            _loading.off();
            _toast.show('Calificación enviada', '', '', 'bottom', 3000);
            navMenuPrincipal('', true, 'fade');
        }
    }, function (j) {
        _loading.off();
        uMensajes.showError('Error al enviar encuesta', 1, false);
    }); 
}