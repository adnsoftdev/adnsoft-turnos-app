var calendario = {
    cDesde: null,
    cHasta: null,
    cData: null,
    /**
     * @param {Date} desde
     * @param {Date} hasta
     */
    inicializar: function (desde, hasta, data) {
        var modificador = desde.getDay() * (-1);
        var contador = 0;
        for (var i = 1; i <= 3; i++) {
            for (var j = 1; j <= 7; j++) {
                var selector = 's' + i + 'd' + j;
                var dia = new Date(
                    desde.getFullYear(),
                    desde.getMonth(),
                    desde.getDate() + modificador,
                    desde.getHours(),
                    desde.getMinutes(),
                    desde.getSeconds()
                );
                var texto = (
                    (dia.getDate() < 10 ? ('0' + dia.getDate()) : dia.getDate()) + '/' + 
                    ((dia.getMonth() + 1) < 10 ? ('0' + (dia.getMonth() + 1)) : (dia.getMonth() + 1))
                );
                $('#' + selector).html(
                    '<input type="hidden" id="hd' + selector + '" value="' + texto + '/' + dia.getFullYear() + '"/>' + 
                    '<span>' + texto + '</span>'
                );
                // estilos
                var estiloDia = calendario.evaluarDia(dia, data);
                if (dia < desde || dia > hasta) {
                    $('#td' + selector).addClass('ui-disabled');
                } else {
                    if (estiloDia.tipo == 1 || estiloDia.tipo == 2 || estiloDia.tipo == 3) {
                        window.sessionStorage.setItem('_fec' + selector, JSON.stringify(estiloDia.datos));
                    }
                    switch (estiloDia.tipo) {
                        case 1:
                            $('#td' + selector).addClass('ref_verde');
                            break;
                        case 2:
                            $('#td' + selector).addClass('ref_azul');
                            break;
                        case 3:
                            $('#td' + selector).addClass('ref_naranja');
                            break;
                        default:
                            $('#td' + selector).addClass('ref_normal');
                            break;
                    }
                }
                modificador++;
            }
        }
    },

    /**
     * Evalua que tipo de reserva se va a hacer de el dia pasado por parametro
     * @param {Date} dia Dia a evaluar
     * @param {array} horariosReserv Horarios devueltos por el webservice
     * @returns {number} Tipo de formato
     */
    evaluarDia: function (dia, horariosReserv) {
        var cuenta = [false, false];
        var data = [];
        for (var i = 0; i < horariosReserv.length; i++) {
            var h = horariosReserv[i];
            var fec = uFecha.getDateFromStringDb(h.hora);

            if (fec.getDate() == dia.getDate() && fec.getMonth() == dia.getMonth() && fec.getFullYear() == dia.getFullYear()) {
                data.push(h);
                switch (h.franja) {
                    case 1:
                        cuenta[0] = true;
                        break;
                    case 2:
                        cuenta[1] = true;
                        break;
                    default: 
                        break;
                }
            }
        }
        var dev = {
            tipo: 0,
            datos: data
        };
        if (cuenta[0] && cuenta[1]) {
            // todo el dia
            dev.tipo = 3;
            
        } else {
            if (cuenta[0]) {
                // solo mañana
                dev.tipo = 1;
            } else {
                if (cuenta[1]) {
                    // solo tarde
                    dev.tipo = 2;
                } else {
                    dev.tipo = 0;
                }
            }
        }
        return dev;
    },

    elegirFecha: function (selec) {
        var fechaElegida = $('#hd' + selec).val();
        vars.selFechaElegida = selec;
        if ($('#td' + selec).hasClass('ref_normal')) {
            _toast.show('Turnos no disponibles para el ' + fechaElegida, '', '', 'center', 'short');
        } else {
           navConfirmarReserva();
        }
    },

    reiniciarCalendario: function () {
        vars.selFechaElegida = '';
        $('#fechaElegidaCalendario').text('');
        $('#btnConfirmarFecha').text('Confirmar fecha');
        $('#dConfirmarFecha').addClass('ui-disabled');
        $('#dReferenciasCalendario').removeClass('ui-disabled');
        $('thead').removeClass('ui-disabled');
        $('#btnConfirmarFecha').removeClass('ui-btn-c').addClass('ui-btn-a');
        $('#btnModificarFecha').removeClass('ui-btn-d').addClass('ui-btn-a');
        for (var i = 1; i <= 3; i++) {
            for (var j = 1; j <= 7; j++) {
                if ($('#tds' + i + 'd' + j).hasClass('ui-disabled')) {
                    $('#tds' + i + 'd' + j).removeClass('ui-disabled');
                }
                if ($('#tds' + i + 'd' + j).hasClass('texto_pantalla_w')) {
                    $('#tds' + i + 'd' + j).removeClass('texto_pantalla_w');
                }
            }
        }
        this.inicializar(this.cDesde, this.cHasta, this.cData);
    }
}