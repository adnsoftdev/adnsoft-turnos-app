/**
 * Controla los pacientes elegidos en cuanto a las agendas de cada uno. Muestra una confirmación
 * si algun paciente no tiene agendas.
 */
function validarPacientes() {
    _agendas.validarDiasCoincidentes(function () {
        var tipoReserva = window.sessionStorage.getItem('_tipoReserva');
        var usuariosSinReserva = '';
        if (tipoReserva == '1') {
            if (obtenerCantidadAgendasPorPaciente(vars.usuarioLogueado.id) == 0) {
                usuariosSinReserva += '- ' + uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado) + '\n';
            }
        }
        for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
            var idTemp = vars.usuariosTurno.grupoPacienteTitular[i].id;
            if (obtenerCantidadAgendasPorPaciente(idTemp) == 0) {
                usuariosSinReserva += '- ' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '\n';
            }
        }

        if (usuariosSinReserva == '') {
            // navElegirQuincena(false);
            navElegirHorarios(false, 'pop');
        } else {
            navigator.notification.confirm(
                'Los siguientes pacientes todavía no tienen turnos elegidos: \n' + usuariosSinReserva + '\nDesea continuar de todas formas?',
                function (n) {
                    if (n == 1) {
                        // navElegirQuincena(false);
                        navElegirHorarios(false, 'pop');
                    }
                }, 'Información', ['Continuar', 'Seguir aquí']);
        }
    }, function () {
        navigator.notification.alert('No se encontró ningún día que coincidan todas las agendas', null, 'Advertencia', 'Revisar');
    });
}

/**
 * Obtiene la cantidad de agendas elegidas hasta el momento de un paciente, por su ID.
 * @param {number} pacienteId ID del paciente a buscar
 */
function obtenerCantidadAgendasPorPaciente(pacienteId) {
    var contador = 0;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        if (vars.pacientesTurno[i].pacienteId == pacienteId) {
            contador++;
        }
    }
    return contador;
}

function navElegirHorarios(r, t) {
    navUtil.to('vElegirHorario.html', '#pElegirHorario', t, r, function () {
        window.sessionStorage.setItem('_vista', 'elegirHorario');

        if (vars.horaDesde != null && vars.horaHasta != null) {
            $('#iHoraDesde').val( (vars.horaDesde.getHours() < 10) ? ('0' + vars.horaDesde.getHours()) : (vars.horaDesde.getHours()) );
            $('#iMinutoDesde').val( (vars.horaDesde.getMinutes() < 10) ? ('0' + vars.horaDesde.getMinutes()) : (vars.horaDesde.getMinutes()) );
            $('#iHoraHasta').val( (vars.horaHasta.getHours() < 10) ? ('0' + vars.horaHasta.getHours()) : (vars.horaHasta.getHours()) );
            $('#iMinutoDesde').val( (vars.horaHasta.getMinutes() < 10) ? ('0' + vars.horaHasta.getMinutes()) : (vars.horaHasta.getMinutes()) );
        }
    });
}

function validarHorarios(e) {
    if (e != null && e != undefined) {
        e.preventDefault();
    }

    var horaDesde = $('#iHoraDesde').val();
    var minutoDesde = $('#iMinutoDesde').val();
    var horaHasta = $('#iHoraHasta').val();
    var minutoHasta = $('#iMinutoHasta').val();

    var fechaActual = new Date();
    var momentoDesde = new Date(
        fechaActual.getFullYear(),
        fechaActual.getMonth(),
        fechaActual.getDate(),
        horaDesde, minutoDesde, 0, 0
    );
    var momentoHasta = new Date(
        fechaActual.getFullYear(),
        fechaActual.getMonth(),
        fechaActual.getDate(),
        horaHasta, minutoHasta, 0, 0
    );

    if (momentoDesde < momentoHasta) {
        vars.horaDesde = momentoDesde;
        vars.horaHasta = momentoHasta;
        navElegirQuincena(false, 'slide');
    } else {
        navigator.notification.alert(
            'La hora de inicio debe ser menor a la de fin. \n\nVerifique los datos ingresados',
            null, 'Intervalo incorrecto', 'OK');
    }
}

function navElegirQuincena(r, t) {
    navUtil.to('vElegirQuincena.html', '#pElegirQuincena', t, r, function () {
        StatusBar.backgroundColorByHexString("#0080e0");
        window.sessionStorage.setItem('_vista', 'elegirQuincena');

        var fechaActual = new Date();
        var fechaSiguiente = new Date(
            fechaActual.getFullYear(),
            fechaActual.getMonth(),
            fechaActual.getDate() + 1,
            fechaActual.getHours(),
            fechaActual.getMinutes(),
            fechaActual.getSeconds()
        );

        for (var i = 0; i < 6; i++) {
            var valorDesde = new Date(
                fechaSiguiente.getFullYear(),
                fechaSiguiente.getMonth(),
                fechaSiguiente.getDate(),
                fechaSiguiente.getHours(),
                fechaSiguiente.getMinutes(),
                fechaSiguiente.getSeconds()
            );
            var valorHasta = new Date(
                fechaSiguiente.getFullYear(),
                fechaSiguiente.getMonth(),
                fechaSiguiente.getDate(),
                fechaSiguiente.getHours(),
                fechaSiguiente.getMinutes(),
                fechaSiguiente.getSeconds()
            );
            valorDesde.setDate(valorDesde.getDate() + (14 * i));
            valorHasta.setDate((valorHasta.getDate() - 1) + (14 * (i + 1)));
            $('#quincena' + (i + 1)).text(
                'Desde ' + uFecha.getFechaSinHora(valorDesde) +
                ' hasta ' + uFecha.getFechaSinHora(valorHasta)
            );
            $('#quincena' + (i + 1) + 'desde').val(uFecha.getFechaSinHora(valorDesde));
            $('#quincena' + (i + 1) + 'hasta').val(uFecha.getFechaSinHora(valorHasta));
        }
    });
}

function navCalendario(nroQuincena, dir, r, t) {
    if (dir == 0) {
        var desdeString = $('#quincena' + nroQuincena + 'desde').val();
        var hastaString = $('#quincena' + nroQuincena + 'hasta').val();
        vars.quincenaDesde = desdeString;
        vars.quincenaHasta = hastaString;
        vars.nroQuincena = nroQuincena;
    }
    var desdeDate = uFecha.getFechaDate(vars.quincenaDesde);
    var hastaDate = uFecha.getFechaDate(vars.quincenaHasta);
    var agendasEnviar = '';
    var envio = { desde: '', hasta: '', agendas: '' };

    envio.desde = uFecha.getFechaStringDBsinHora(desdeDate);
    envio.hasta = uFecha.getFechaStringDBsinHora(hastaDate);

    var agId = -1;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        if (agId != vars.pacientesTurno[i].agendaId) {
            agId = vars.pacientesTurno[i].agendaId;
            // envio.agendas += (i != 0 ? ',' : '') + agId;
            agendasEnviar += (i != 0 ? ',' : '') + agId;
        }
    }
    
    _loading.on(vars.quincenaDesde + ' - ' + vars.quincenaHasta + '\n\n'
        + 'Aguarde unos instantes mientras buscamos '
        + 'las mejores fechas para obtener todos los turnos en un mismo día');
    _turnos.getTurnosLibres(desdeDate, hastaDate, agendasEnviar, function (d) {
        var arrayExito = [];
        var arrayTurnosLibresTotal = [];

        /* se crea el array general de turnos libres */
        /* >> tiene en cuenta que los turnos estén en el rango de horario elegido */
        for (var i = 0; i < d.length; i++) {
            for (var j = 0; j < d[i].disponible.length; j++) {
                for (var k = 0; k < d[i].disponible[j].libres.length; k++) {
                    var arrayHora = d[i].disponible[j].libres[k].hora.split(':');
                    var horaTurno = new Date();
                    horaTurno.setFullYear(vars.horaDesde.getFullYear());
                    horaTurno.setMonth(vars.horaDesde.getMonth());
                    horaTurno.setDate(vars.horaDesde.getDate());
                    horaTurno.setHours(arrayHora[0]);
                    horaTurno.setMinutes(arrayHora[1]);
                    horaTurno.setSeconds(arrayHora[2]);
                    if (horaTurno >= vars.horaDesde && horaTurno <= vars.horaHasta) {
                        var turnoLibre = {
                            agenda: d[i].agenda,
                            agendaNombre: d[i].nombre,
                            agendaTipo: d[i].tipo,
                            fecha: d[i].disponible[j].fecha,
                            hora: d[i].disponible[j].libres[k].hora,
                            fechaHora: uFecha.getDateFromStringDb(d[i].disponible[j].fecha + ' ' + d[i].disponible[j].libres[k].hora),
                            horarioId: d[i].disponible[j].libres[k].horarioId,
                            franja: d[i].disponible[j].libres[k].franja,
                            ocupado: false
                        };
                        arrayTurnosLibresTotal.push(turnoLibre);
                    }
                }
            }
        }

        /* si hay turnos libres */
        if (arrayTurnosLibresTotal.length > 0) {
            // ordeno el array de turnos libres por fecha/hora
            arrayTurnosLibresTotal.sort(function (a, b) {
                return a.fechaHora - b.fechaHora;
            });

            /* agregar campo en vars.pacientesTurno que contenta todos los dias y horarios minimos */
            /* >> por ahora valores de inicialización */
            for (var f = 0; f < vars.pacientesTurno.length; f++) {
                vars.pacientesTurno[f].asignaciones = [];
                for (var ff = 0; ff < 14; ff++) {
                    var diaAsign = new Date(desdeDate.getFullYear(), desdeDate.getMonth(), (desdeDate.getDate() + ff),
                                            desdeDate.getHours(), desdeDate.getMinutes(), desdeDate.getSeconds());
                    vars.pacientesTurno[f].asignaciones.push({
                        dia: diaAsign, 
                        minimo: _turnos.getMinimoHorarioDia(vars.pacientesTurno[f].agendaId, diaAsign, arrayTurnosLibresTotal)
                    });
                }
            }
            
            // se realiza un bucle de 14 (que serían los días de la quincena)
            for (var i = 0; i < 14; i++) {

                /* realizar ordenamiento por PRIORIDAD de agenda (en base al horario más teprano posible) */
                vars.pacientesTurno.sort(function (a, b) {
                    // ver como se puede ordenar en base a campos que son null (dias de fin de semana)
                    return a.asignaciones[i].minimo.fechaHora - b.asignaciones[i].minimo.fechaHora;
                });

                /* ordenar array vars.pacientesTurno por paciente y tipoagenda */
                vars.pacientesTurno.sort(function (a, b) {
                    var aPacienteId = a.pacienteId;
                    var bPacienteId = b.pacienteId;
                    var aTipo = a.agendaTipo;
                    var bTipo = b.agendaTipo;
                    if (aPacienteId == bPacienteId) {
                        return (aTipo > bTipo) ? -1 : (aTipo < bTipo) ? 1 : 0;
                    } else {
                        return (aPacienteId < bPacienteId) ? -1 : 1;
                    }
                });

                var desdeDateModif = new Date(
                    desdeDate.getFullYear(),
                    desdeDate.getMonth(),
                    desdeDate.getDate() + i,
                    desdeDate.getHours(),
                    desdeDate.getMinutes(),
                    desdeDate.getSeconds()
                );
                var horaUltimoTurnoEstudio = { pac: 0, fec: new Date(1900, 0, 1) };
                var contadorTurnosDelDia = 0;
                for (var j = 0; j < vars.pacientesTurno.length; j++) {
                    var idp = vars.pacientesTurno[j].pacienteId;
                    var agn = vars.pacientesTurno[j].agendaId;
                    var atp = vars.pacientesTurno[j].agendaTipo;
                    var obs = vars.pacientesTurno[j].agendaObservaciones;
                    for (var k = 0; k < arrayTurnosLibresTotal.length; k++) {
                        turnoLibre = arrayTurnosLibresTotal[k];
                        
                        if (turnoLibre.agenda == agn && turnoLibre.agendaTipo == atp) {
                            /* condiciones de inserción */
                            var fechaOK = uFecha.mismaFecha(turnoLibre.fechaHora, desdeDateModif);
                            var ultimoTurnoPac = obtenerUltimoTurnoPaciente(idp, turnoLibre.fechaHora, arrayExito);
                            var intervaloPacOK = (ultimoTurnoPac != null ? (turnoLibre.fechaHora - uFecha.getDateFromStringDb(ultimoTurnoPac.hora) >= 1800000) : true);
                            var profValido = ((turnoLibre.fechaHora > horaUltimoTurnoEstudio.fec || (idp != horaUltimoTurnoEstudio.pac && horaUltimoTurnoEstudio.pac > 0)) && atp == 1);
                            
                            if (fechaOK && intervaloPacOK && ( (atp == 2) || profValido ) && turnoLibre.ocupado == false) {
                                arrayExito.push({
                                    agendaId: turnoLibre.agenda,
                                    horarioId: turnoLibre.horarioId,
                                    hora: turnoLibre.fecha + ' ' + turnoLibre.hora,
                                    franja: turnoLibre.franja,
                                    pacId: idp,
                                    observaciones: obs
                                });
                                
                                contadorTurnosDelDia++;
                                if ( (turnoLibre.fechaHora > horaUltimoTurnoEstudio.fec) && atp == 2 ) {
                                    horaUltimoTurnoEstudio.fec = turnoLibre.fechaHora;
                                    if (idp != horaUltimoTurnoEstudio.pac) {
                                        horaUltimoTurnoEstudio.pac = idp;
                                    }
                                }
                                arrayTurnosLibresTotal[k].ocupado = true;
                                break;
                            }
                        }
                    }
                    if (j == (vars.pacientesTurno.length - 1)) {
                        if (contadorTurnosDelDia < vars.pacientesTurno.length) {
                            // eliminar los ingresados
                            for (var c = 0; c < contadorTurnosDelDia; c++) {
                                arrayExito.pop();
                            }
                            contadorTurnosDelDia = 0;
                        }
                    }
                }                
            }
            
            if (arrayExito.length > 0) {
                _loading.off();
                navUtil.to('vCalendario.html', '#pCalendario', t, r, function () {
                    StatusBar.backgroundColorByHexString("#0080e0");
                    _loading.off();
                    window.sessionStorage.setItem('_vista', 'calendario');
                    $('.aElegirDia').click(function (e) {
                        var idFecha = $(e.currentTarget).attr('id');
                        calendario.elegirFecha(idFecha);
                    });
                    $('#sDesde').text(vars.quincenaDesde);
                    $('#sHasta').text(vars.quincenaHasta);
                    calendario.cDesde = desdeDate;
                    calendario.cHasta = hastaDate;
                    calendario.cData = arrayExito;
                    calendario.inicializar(desdeDate, hastaDate, arrayExito);
                });
            } else {
                _loading.off();
                navigator.notification.confirm(
                    'No hay horarios disponibles para las combinaciones elegidas.\n\nOpciones:',
                    function (opc) {
                        switch (opc) {
                            case 1:
                                // navegar a lista de pacientes
                                navElegirPaciente(null, true, 'pop');
                                break;
                            case 2:
                                // navegar a elegir horarios
                                navElegirHorarios(true, 'slide');
                                break;
                            case 3:
                                // quedarse
                                break;
                        }
                    }, 'Advertencia', ['Revisar agendas', 'Cambiar horario', 'Cambiar quincena']);
            }
        } else {
            _loading.off();
            navigator.notification.alert('No hay horarios disponibles para las combinaciones elegidas.', function () {
                navElegirHorarios(true, 'slide');
            }, 'Advertencia', 'Reintentar con otro horario');
        }
    });
}

/**
 * Obtiene el menor turno posible para una agenda en determinada fecha
 * @param {number} idAgenda 
 * @param {Date} fecha 
 * @param {Array} arrTurnos 
 */
function obtenerAgendaMenorTurno(idAgenda, fecha, arrTurnos) {
    var turno = null;
    var fechaControl = new Date(fecha.getFullYear() + 5, fecha.getMonth(), fecha.getDate());
    for (var i = 0; i < arrTurnos.length; i++) {
        var fechaArray = arrTurnos[i].fechaHora;
        var fechaEsValida = fechaArray.getFullYear() == fecha.getFullYear() 
                            && fechaArray.getMonth() == fecha.getMonth() 
                            && fechaArray.getDate() == fecha.getDate();
        if (fechaEsValida && arrTurnos[i].agenda == idAgenda) {
            if (fechaArray < fechaControl) {
                turno = arrTurnos[i];
                fechaControl = fechaArray;
            }
        }
    }
    return turno;
}

function obtenerUltimoTurnoPaciente(idp, fecha, arr) {
    var fechaMin = new Date(1900,0,1);
    var ultimoTurno = null;
    for (var a = 0; a < arr.length; a++) {
        var fechaTurno = uFecha.getDateFromStringDb(arr[a].hora);
        if (fechaTurno.getFullYear() == fecha.getFullYear() && fechaTurno.getMonth() == fecha.getMonth() && fechaTurno.getDate() == fecha.getDate()) {
            if (arr[a].pacId == idp) {
                if (fechaTurno > fechaMin) {
                    fechaMin = fechaTurno;
                    ultimoTurno = arr[a];
                }
            }
        }
    }
    return ultimoTurno;
}

function reiniciarCalendario() {
    calendario.reiniciarCalendario();
}

function navConfirmarReserva() {
    navUtil.to('vConfirmarReserva.html', '#pConfirmarReserva', 'fade', false, function () {
        window.sessionStorage.setItem('_vista', 'confirmarReserva');
        crearListaTurnosPedidos('#pConfirmarReserva');
    });
}

/**
 * Crea visualmente la lista de turnos pedidos. Se usa en la confirmación y en la finalización.
 * @param {string} container Selector que indica cuál es el elemento padre.
 */
function crearListaTurnosPedidos(container) {
    var dataFechaElegida = [];
    dataFechaElegida = JSON.parse(window.sessionStorage.getItem('_fec' + vars.selFechaElegida));
    dataFechaElegida.sort(function (a, b) {
        if (a.hora < b.hora)
            return -1;
        if (a.hora > b.hora)
            return 1;
        return 0;
    });
    StatusBar.backgroundColorByHexString("#f9f9f9");
    StatusBar.styleDefault();
    for (var i = 0; i < dataFechaElegida.length; i++) {
        // formato: yyyy-MM-dd HH:mm:ss
        var dato = dataFechaElegida[i].hora;
        var fechaHoraArray = dato.split(' '); // 'T'
        var horaMostrar = uFecha.getHoraCorta(fechaHoraArray[1]);
        var fechaddmmyyy = uFecha.getFechaFormatoNormal(fechaHoraArray[0]);
        var fechaMostrar = uFecha.getFechaFormatoTexto(uFecha.getFechaDate(fechaddmmyyy));
        $(container + ' #h3FechaReserva').text(fechaMostrar);
        vars.fechaReservaTexto = fechaMostrar;
        vars.reservaEnviar.id = vars.usuarioLogueado.id;
        vars.reservaEnviar.data.push({
            pacId: dataFechaElegida[i].pacId,
            fecha: fechaHoraArray[0],
            // hora: fechaHoraArray[1].split('-')[0],
            hora: fechaHoraArray[1],
            horarioId: dataFechaElegida[i].horarioId,
            observaciones: dataFechaElegida[i].observaciones
        });

        var descripcionMostrar = '';
        descripcionMostrar += '<li>'
            + '<div>'
            + '<h1 class="texto_pantalla texto_22">' + horaMostrar + ' hs.</h1>';

        var nombrePac = '';
        var nombreAg = '';
        if (dataFechaElegida[i].pacId == vars.usuarioLogueado.id) {
            nombrePac = uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado);
        } else {
            for (var j = 0; j < vars.usuariosTurno.grupoPacienteTitular.length; j++) {
                if (dataFechaElegida[i].pacId == vars.usuariosTurno.grupoPacienteTitular[j].id) {
                    nombrePac = uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[j]);
                    break;
                }
            }
        }
        var agendasDisp = JSON.parse(window.localStorage.getItem('_agendas'));
        for (var k = 0; k < agendasDisp.length; k++) {
            if (dataFechaElegida[i].agendaId == agendasDisp[k].agendaId) {
                nombreAg = agendasDisp[k].nombre + (dataFechaElegida[i].observaciones != '' ? (' (' + dataFechaElegida[i].observaciones + ')') : '');
                break;
            }
        }

        // descripcionMostrar += '<span>' + nombrePac + ' (' + nombreAg + ')' + '</span></li>';
        descripcionMostrar += '<h1 class="agenda_resumen wrap">' + nombreAg + '</h1>'
            + '</div>';
        descripcionMostrar += '<p>Paciente</p>';
        descripcionMostrar += '<h1>' + nombrePac + '</h1>';
        // descripcionMostrar += '<p>Turno:</p><h1 class="agenda_resumen">' + nombreAg + '</h1></li>';
        $(container + ' #dDescripcionReserva').append(descripcionMostrar);
        $(container + ' #dDescripcionReserva').listview('refresh');
    }
}

function cambiarDiaReserva() {
    vars.reservaEnviar.data = [];
    vars.pacientesEnviar.desde = '';
    vars.pacientesEnviar.data = [];
    vars.selFechaElegida = '';
    window.sessionStorage.removeItem('_fec' + vars.selFechaElegida);
    navCalendario(0, 1, true, 'fade');
}

function cambiarQuincenaReserva() {
    vars.reservaEnviar.data = [];
    vars.pacientesEnviar.desde = null;
    vars.pacientesEnviar.data = [];
    vars.selFechaElegida = '';
    window.sessionStorage.removeItem('_fec' + vars.selFechaElegida);
    navElegirQuincena(true, 'pop');
}

function enviarReserva() {
    _loading.on('Enviando...');
    var reserva = vars.reservaEnviar;
    ws.call(vars.url + '/reservar-turnos', 'POST', reserva, function (obj, err, st) {
        _loading.off();
        navigator.notification.alert('Ha ocurrido un error. Reintente de nuevo más tarde', null, '', 'Entendido');
    }, function (d) {
        _loading.off();
        navUtil.to('vFinalizarReserva.html', '#pFinalizarReserva', 'slideup', false, function () {
            window.sessionStorage.setItem('_vista', 'finalizarReserva');
            crearListaTurnosPedidos('#pFinalizarReserva');
            // alert(JSON.stringify(vars.reservaEnviar));
            // _notification.show()
        });
    });
}

function finalizarReserva() {
    navMenuPrincipal('../', true, 'slideup');
    vars.reiniciarDatos();
}

function cancelarReservaCompleta() {
    finalizarReserva();
    _toast.show('Reserva cancelada', '', '', 'center', 1500);
}

function confirmCancelarReservaCompleta() {
    navigator.notification.confirm('¿Cancelar reserva?', function (nn) {
        if (nn == 1) {
            cancelarReservaCompleta();
        }
    }, '', ['SI', 'NO']);
}