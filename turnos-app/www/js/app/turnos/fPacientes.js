function navElegirPaciente(callback, r, t) {
    // vars.pacientesEnviar.desde = '';
    // vars.pacientesEnviar.data = [];
    navUtil.to('vElegirPaciente.html', '#pElegirPaciente', t, r, function () {
        window.sessionStorage.setItem('_vista', 'elegirPaciente');
        crearListaPacientes(callback);
    });
}

function crearListaPacientes(callback) {
    if (vars.pacientesTurno.length == 0) {
        $('#btnTerminarReserva').addClass('ui-disabled');
        $('#btnDetalleReserva').addClass('ui-disabled');
    } else {
        $('#btnTerminarReserva').removeClass('ui-disabled');
        $('#btnDetalleReserva').removeClass('ui-disabled');
    }
    var tipoReserva = parseInt(window.sessionStorage.getItem('_tipoReserva'));
    var htmlPacientesElegir = '';
    $('#dPacientesCargados').empty();
    switch (tipoReserva) {
        case 0:
        case 1:
            $('#dTextoSinPacientes').hide();
            htmlPacientesElegir += 
                '<div data-role="controlgroup" data-type="horizontal" class="fila_paciente">' +
                '<a class="ui-btn ui-btn-a paciente" onclick="seleccionarPaciente(' + vars.usuarioLogueado.id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado) + '\')">' + uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado) 
                + '</a>' +
                '<a class="ui-btn ui-btn-c opcion" onclick="crearListaAgendasPacientePopup(' + vars.usuarioLogueado.id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuarioLogueado) + '\')"><strong class="texto_pantalla">' + obtenerCantidadAgendasPorPaciente(vars.usuarioLogueado.id) 
                + '</strong></a>' +
                '<a class="ui-btn ui-disabled ui-btn-a opcion ui-nodisc-icon ui-btn-icon-notext ui-alt-icon ui-icon-edit" onclick="editarPaciente(' + vars.usuarioLogueado.id + ')">O' 
                + '</a>' +
                '</div>';
                
            for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
                htmlPacientesElegir +=
                    '<div data-role="controlgroup" data-type="horizontal" class="fila_paciente">' +
                    '<a class="ui-btn ui-btn-a paciente" onclick="seleccionarPaciente(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '\')">' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '</a>' +
                    '<a class="ui-btn ui-btn-c opcion" onclick="crearListaAgendasPacientePopup(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '\')"><strong class="texto_pantalla">' + obtenerCantidadAgendasPorPaciente(vars.usuariosTurno.grupoPacienteTitular[i].id) + '</strong></a>' +
                    '<a class="ui-btn ui-btn-a opcion ui-nodisc-icon ui-btn-icon-notext ui-alt-icon ui-icon-edit" onclick="editarPaciente(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ')">O</a>' +
                    '</div>';
            }
            break;
        case 2:
            if (vars.usuariosTurno.grupoPacienteTitular.length > 0) {
                $('#dTextoSinPacientes').hide();
                for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
                    htmlPacientesElegir +=
                        '<div data-role="controlgroup" data-type="horizontal" class="fila_paciente">' +
                        '<a class="ui-btn ui-btn-a paciente" onclick="seleccionarPaciente(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '\')">' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '</a>' +
                        '<a class="ui-btn ui-btn-c opcion" onclick="crearListaAgendasPacientePopup(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ', \'' + uCadenas.getPacienteNombreCompleto(vars.usuariosTurno.grupoPacienteTitular[i]) + '\')"><strong class="texto_pantalla">' + obtenerCantidadAgendasPorPaciente(vars.usuariosTurno.grupoPacienteTitular[i].id) + '</strong></a>' +
                        '<a class="ui-btn ui-btn-a opcion ui-nodisc-icon ui-btn-icon-notext ui-alt-icon ui-icon-edit" onclick="editarPaciente(' + vars.usuariosTurno.grupoPacienteTitular[i].id + ')">O</a>' +
                        '</div>';
                }
            } else {
                $('#dTextoSinPacientes').show();
            }
            
            break;
        default:
            navUtil.to('../vError.html', '#pError', 'fade', false, function () {
                $('#descripcionError').text('Ocurrió un error durante la reserva. Contáctese con el administrador');
            });
            break;
    }
    window.sessionStorage.setItem('_ordenPacientes', 0);

    $('#dPacientesCargados').append(htmlPacientesElegir).each(function (ind, ele) {
        $('.fila_paciente').controlgroup().controlgroup('refresh');
        $('.ui-controlgroup-controls').addClass('center_text').addClass('ui-shadow');
    });
    if (callback != null && callback != undefined) {
        callback();
    }
}



function iniciarIngresoPacientes() {
    var checked = $('div :checked').val();
    if (checked == null) {
        uMensajes.showError('No se ha elegido la cantidad de pacientes', 2, false);
    } else {
        window.sessionStorage.setItem('_cantidadPersonasTurno', checked);
        window.sessionStorage.setItem('_ordenPacientes', 0);

        vars.usuariosTurno.grupoPacienteTitular = [];

        // hacer validación sobre en qué orden de paciente va ingresando
        navUtil.to('vIngresoIDPacientes.html', '#pIngresoIDPacientes', 'slide', false, navIngresoIDPacientes);
    }
}

function navIngresoIDPacientes() {
    window.sessionStorage.setItem('_vista', 'ingresoIDPacientes');
    // $('#dBotonesIDPacientes').hide();
    // $('#nombreIDPaciente').hide();
    $('#txtIDPaciente').removeClass('ui-disabled');
    $('#btnConfirmarIDPaciente').removeClass('ui-disabled');
    $('#txtFecPaciente').removeClass('ui-disabled');
    var totalPacientes = parseInt(window.sessionStorage.getItem('_cantidadPersonasTurno'));
    var orden = parseInt(window.sessionStorage.getItem('_ordenPacientes'));
    vars.tipoFiltroBusquedaPaciente = 1;
    orden++;
    if (orden <= totalPacientes) {
        $('#sOrdenPersonaCuerpo').text(orden);
        $('#hProgresoFooter').text('Paciente ' + orden + ' de ' + totalPacientes);
        $('#txtIDPaciente').val('');
        // $('#txtIDPaciente').blur();
        // $('#txtIDPaciente').focus();
        // $('#txtFecPaciente').val('');
        $('#tFechaIngresoPacientes #anio').val('');
        $('#tFechaIngresoPacientes #mes').val('');
        $('#tFechaIngresoPacientes #dia').val('');
        $('#tFechaIngresoPacientes #anio').blur();
        $('#tFechaIngresoPacientes #mes').blur();
        $('#tFechaIngresoPacientes #dia').blur();
        window.sessionStorage.setItem('_ordenPacientes', orden);
    } else {
        navElegirPaciente(checkPrimerUso, false);
    }
}

function modificarTipoFiltroPaciente(tipo) {
    $('#txtIDPaciente').val('');
    switch (tipo) {
        case 1:
            $('#txtIDPaciente').attr('placeholder', 'ID de paciente...');
            vars.tipoFiltroBusquedaPaciente = 1;
            break;
        case 2:
            $('#txtIDPaciente').attr('placeholder', 'D.N.I. de paciente...');
            vars.tipoFiltroBusquedaPaciente = 2;
            break;
        default:
            break;
    }
}

function buscarPaciente(e) {
    if (e != undefined && e != null) {
        e.preventDefault();
    }
    var pacienteBuscar = $('#txtIDPaciente').val();
    var dia = $('#tFechaIngresoPacientes #dia').val();
    var mes = $('#tFechaIngresoPacientes #mes').val();
    var anio = $('#tFechaIngresoPacientes #anio').val();
    if ( dia == '' || mes == '' || anio == '' || pacienteBuscar == '') {
        navigator.notification.alert('Complete todos los campos antes de continuar', null, '', 'Entendido');
        return;
    } else {
        if (moment(anio + '-' + mes + '-' + dia).isValid() == false) {
            uMensajes.showError('Fecha incorrecta', 2, false);
            listenersObject.inputFechaTotalListener('tFechaIngresoPacientes')
            return;
        }
    }

    var fecPacienteBuscar = anio + '-' + mes + '-' + dia;
    
    if (parseInt(pacienteBuscar) == NaN || pacienteBuscar.length == 0) {
        uMensajes.showError('Ingrese un número válido', 2, false);
    } else {
        var usr = false;
        for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
            if (pacienteBuscar == vars.usuariosTurno.grupoPacienteTitular[i].id) {
                usr = true;
                break;
            }
        }
        if (pacienteBuscar != vars.usuarioLogueado.id && !usr) {
            // consulta al webservice
            _loading.on('Cargando...');
            switch (vars.tipoFiltroBusquedaPaciente) {
                case 1: 
                    _pacientes.getByIdFecha(
                        pacienteBuscar, fecPacienteBuscar, 
                        busquedaPacienteExito, busquedaPacienteError
                    );
                    break;
                case 2:
                    _pacientes.getByDocumentoFecha(
                        pacienteBuscar, fecPacienteBuscar,
                        busquedaPacienteExito, busquedaPacienteError
                    );
                    break;
            }
        } else {
            uMensajes.showError('El usuario ya se ha agregado anteriormente. \nIntente con otros datos', 1, false);
        }
    }
    // es para que no ejecute el action del form por defecto
    return false;
}

function busquedaPacienteExito(data) {
    _loading.off();
    window.scrollTo(0, window.outerHeight);
    vars.pacienteTemp = data;
    $('#txtIDPaciente').blur();
    $('#nombreIDPaciente').text(uCadenas.getPacienteNombreCompleto(data));
    navigator.notification.confirm(uCadenas.getPacienteNombreCompleto(data), function (o) {
        if (o == 1) {
            confirmarPacienteIngresado();
        }
        else {
            document.getElementById('txtIDPaciente').focus();
        }
    }, 'Resultado:', ['Es correcto', 'Modificar']);
}

function busquedaPacienteError(j) {
    _loading.off();
    if (j.status == 500) {
        uMensajes.showError('Paciente no encontrado', 1, false);
    } else {
        uMensajes.showError('Error de comunicación con el servidor', 1, false);
    }
    $('#txtIDPaciente').focus();
}

function confirmarPacienteIngresado() {
    // var obj = {};
    // obj.idPaciente = vars.idPacienteTemp;
    // obj.nombrePaciente = vars.nombrePacienteTemp;
    vars.usuariosTurno.grupoPacienteTitular.push(vars.pacienteTemp);
    vars.pacienteTemp = null;
    $('#popupPacienteEncontrado').popup('close');
    navIngresoIDPacientes();
}





function editarPaciente(idPaciente) {
    navigator.notification.confirm('Acciones:', function (ac) {
        switch (ac) {
            case 1:
                // editar paciente
                navigator.notification.prompt('Ingrese ID nuevo paciente:', function (res) {
                    if (res.buttonIndex == 1) {
                        if (res.input1 != '0' && res.input1.length > 0) {
                            _loading.on('Buscando...');
                            ws.call(vars.url + '/get-persona-id/' + res.input1, 'GET', {}, function (j, text, error) {
                                _loading.off();
                                uMensajes.showError('Paciente no encontrado', 1, false);
                                $('#txtIDPaciente').focus();
                            }, function (data) {
                                _loading.off();
                                navigator.notification.confirm(data.nombre + ' ' + data.apellido, function (opc) {
                                    if (opc == 1) {
                                        for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
                                            if (vars.usuariosTurno.grupoPacienteTitular[i].id == idPaciente) {
                                                vars.usuariosTurno.grupoPacienteTitular[i] = data;
                                                break;
                                            }
                                        }
                                        var pacientesTurnoAux = [];
                                        for (var j = 0; j < vars.pacientesTurno.length; j++) {
                                            if (idPaciente != vars.pacientesTurno[j].pacienteId) {
                                                pacientesTurnoAux.push(vars.pacientesTurno[j]);
                                            }
                                        }
                                        vars.pacientesTurno = pacientesTurnoAux;
                                        crearListaPacientes();
                                    }
                                }, 'Paciente encontrado:', ['Aceptar', 'Cancelar']);
                            });
                        } else {
                            uMensajes.showError('ID de paciente no válido', 1, false);
                        }
                    }
                }, '', ['Buscar', 'Cancelar'], '0');
                break;
            case 2:
                var msg = 'Desea eliminar el paciente del pedido de turno?\n'
                    + 'Tenga en cuenta que también se borrarán las agendas elegidas para este paciente.'
                navigator.notification.confirm(msg, function (act) {
                    if (act == 1) {
                        // eliminar
                        for (var i = 0; i < vars.usuariosTurno.grupoPacienteTitular.length; i++) {
                            if (vars.usuariosTurno.grupoPacienteTitular[i].id == idPaciente) {
                                vars.usuariosTurno.grupoPacienteTitular.splice(i, 1);
                                break;
                            }
                        }
                        var pacientesTurnoAux = [];
                        for (var j = 0; j < vars.pacientesTurno.length; j++) {
                            if (idPaciente != vars.pacientesTurno[j].pacienteId) {
                                pacientesTurnoAux.push(vars.pacientesTurno[j]);
                            }
                        }
                        vars.pacientesTurno = pacientesTurnoAux;
                        crearListaPacientes();
                    }
                }, 'Atención', ['Eliminar', 'Cancelar']);
                break;
            case 3:
                break;    
        }
    }, '', ['Modificar paciente', 'Eliminar paciente', 'Cancelar']);
}

function crearListaAgendasPacientePopup(id, nombre) {
    $('#ulAgendasPaciente').empty();
    $('#h3PopupTituloNombrePac').text(nombre);
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        var p = vars.pacientesTurno[i];
        if (p.pacienteId == id) {
            $('#ulAgendasPaciente').append(
                '<li class="ui-nodisc-icon ui-alt-icon"><a>' 
                    + p.agendaNombre 
                    + (p.agendaObservaciones != '' ? '<p class="texto_cursiva texto_14 texto_pantalla_gr wrap">' + p.agendaObservaciones + '</p>' : '')
                    + '<p>' + uCadenas.getDiasAgendasAbv(p.agendaDias) + '</p></a>' +
                '<a onclick="borrarAgendaPaciente(' + p.pacienteId + ', ' + p.agendaId + ')"></a></li>'
            );
            $('#ulAgendasPaciente').listview('refresh');
        }
    }
    $('#popupAgendasPaciente').popup('open');
}

function borrarAgendaPaciente(idp, ida) {
    var objeto = vars.pacientesTurno;
    var indiceBorrar = 0;
    var textoAgenda = '';
    for (var i = 0; i < objeto.length; i++) {
        if (objeto[i].pacienteId == idp && objeto[i].agendaId == ida) {
            indiceBorrar = i;
            textoAgenda = objeto[i].agendaNombre;
            break;
        }
    }
    navigator.notification.confirm('Eliminar agenda "' + textoAgenda + '"?', function (num) {
        if (num == 1) {
            vars.pacientesTurno.splice(indiceBorrar, 1);
            crearListaAgendasPacientePopup(idp);
            crearListaPacientes(null);
        }
    }, '', ['SI', 'NO']);
}


function checkPrimerUso() {
    var ayudaPacientes = window.localStorage.getItem('_ayudaPacientes');
    if (ayudaPacientes == null && parseInt(ayudaPacientes) != 1) {
        var textoAyudaPacientes = '';
        textoAyudaPacientes += '> Para comenzar, toque sobre el nombre de un paciente para buscar una agenda médica.';
        textoAyudaPacientes += '\n\n> Toque sobre el botón azul para gestionar sus agendas.';
        textoAyudaPacientes += '\n\n> Podrá cambiar o quitar un paciente tocando el botón a la derecha de cada uno.';
        textoAyudaPacientes += '\n\nPara ver esta ayuda de nuevo más tarde, presione el botón "Ayuda"';
        navigator.notification.alert(textoAyudaPacientes, function () {
            window.localStorage.setItem('_ayudaPacientes', 1);
        }, 'Información', 'Entendido');
    }
}