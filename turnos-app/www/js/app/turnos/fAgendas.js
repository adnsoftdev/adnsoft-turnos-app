function seleccionarPaciente(idPac, nombrePac) {
    vars.pacienteAgenda.pacienteId = idPac;
    vars.pacienteAgenda.pacienteNombre = nombrePac;
    navElegirAtencion(idPac, nombrePac, false, 'pop');
}

function navElegirAtencion(idPac, nombrePac, r, t) {
    vars.pacienteAgenda.pacienteId = idPac;
    vars.pacienteAgenda.pacienteNombre = nombrePac;
    if (nombrePac != null && nombrePac != undefined && nombrePac != '') {
        vars.pacienteAgenda.pacienteNombre = nombrePac;
    }
    navUtil.to('vElegirTipoAtencion.html', '#pElegirTipoAtencion', t, r, function () {
        window.sessionStorage.setItem('_vista', 'elegirTipoAtencion');
        $('#h3NombreUsuarioTurno').text(vars.pacienteAgenda.pacienteNombre);
    });
}

function verReserva() {
    vars.pacientesTurno.sort(function (a, b) {
        return (a.pacienteId - b.pacienteId);
    });
    var textoReserva = '';
    var idPacTemp = 0;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        var p = vars.pacientesTurno[i];
        if (idPacTemp != p.pacienteId) {
            idPacTemp = p.pacienteId;
            textoReserva += '\nPaciente: ' + p.pacienteNombre + '\n';
        }
        textoReserva += '- ' + p.agendaNombre + (p.agendaObservaciones != '' ? ('(' + p.agendaObservaciones + ')') : '') + '\n';
    }
    navigator.notification.alert((textoReserva.length == 0 ? '[nada todavía]' : textoReserva), null, 'Detalle de turnos', 'OK');
}

function navBusquedaAgenda(elem, t, r) {
    var paciente = vars.pacienteAgenda.pacienteNombre;
    var infoRed = navigator.connection.type;
    if (infoRed == 'none' || infoRed == 'NONE') {
        uMensajes.showError('No hay conexión de red', 1, false);
    } else {
        navUtil.to('vBuscadorAgenda.html', '#pBuscadorAgenda', t, r, function () {
            window.sessionStorage.setItem('_vista', 'buscadorAgenda');
            $('#h3NombreUsuarioTurno2').text(paciente);
            $('#resultadoError').hide();
            switch (parseInt(elem)) {
                case 1:
                    $('#h4TipoAtenc').text('PROFESIONAL');
                    $('#indicacionNoAgendas').hide();
                    $('#ulListaResultados').show();
                    $('#dResultadosBusquedaEsp').hide();
                    vars.pacienteAgenda.agendaTipo = 1;
                    vars.busquedaTipoActual = 1;
                    break;
                case 3:
                    $('#h4TipoAtenc').text('ESPECIALIDAD');
                    $('#cuadroBusquedaAg').hide();
                    $('#dResultadosBusquedaAg').hide();
                    $('#dNombreEspecialidad').hide();
                    $('#ulListaResultadosEsp').show();
                    $('#dResultadosBusquedaEsp').show();
                    vars.pacienteAgenda.agendaTipo = 1;
                    vars.busquedaTipoActual = 2;
                    // _agendas.list() sin parametros para poder guardar las agendas en el localstorage
                    _agendas.list();
                    buscarEspecialidades();
                    break;
                case 2:
                    $('#h4TipoAtenc').text('ESTUDIOS');
                    $('#indicacionNoAgendas').hide();
                    $('#ulListaResultados').show();
                    $('#dResultadosBusquedaEsp').hide();
                    $('#cuadroBusquedaAg').hide();
                    vars.pacienteAgenda.agendaTipo = 2;
                    vars.busquedaTipoActual = 1;
                    buscarAgenda();
                    break;
                default:
                    break;
            }
        });
    }
}

function buscarAgenda() {
    var criterioBusqueda = '';
    var tipo = vars.pacienteAgenda.agendaTipo;
    if (tipo == 1) {
        criterioBusqueda = $('#txtBusquedaAgenda').val();  
        if (criterioBusqueda.length < 1) {
            _loading.off();
            _vibrator.on([50, 50, 50, 50, 50, 50]);
            uMensajes.showError('Debe ingresar como mínimo 1 caracter', 2, false);
            return;
        }
    }
    _loading.on('Buscando...');
    _agendas.list(function (agendas) {
        $('#resultadoError').hide();
        var agendasMostrarArray = [];
        for (var i = 0; i < agendas.length; i++) {
            var agenda = { agendaId: 0, tipoId: 0, nombre: '', profesional: '' };
            agenda.agendaId = agendas[i].agendaId;
            agenda.tipoId = agendas[i].tipoId;
            agenda.nombre = agendas[i].nombre;
            agenda.profesional = agendas[i].profesional;
            agenda.dias = agendas[i].dias;
    
            if (agenda.tipoId == tipo) {
                if (agenda.nombre.indexOf(criterioBusqueda.toUpperCase()) != -1) {
                    agendasMostrarArray.push(agenda);
                }
            }
        }
        _loading.off();
        if (tipo == 2) {
            $('#cuadroBusquedaAg').hide();
        } else {
            $('#cuadroBusquedaAg').show();
            $('#txtBusquedaAgenda').blur();
        }
        if (agendasMostrarArray.length == 0) {
            $('#ulListaResultados').hide();
            $('#indicacionNoAgendas').show();
        } else {
            $('#ulListaResultados').show();
            $('#indicacionNoAgendas').hide();
            $('#ulListaResultados').empty();
            for (var j = 0; j < agendasMostrarArray.length; j++) {
                var contenido = 
                    '<li><a class="ui-btn ui-btn-a wrap" onclick="elegirAgenda(' + agendasMostrarArray[j].agendaId + ')">' 
                    + '<span id="ag' + agendasMostrarArray[j].agendaId + '">' + agendasMostrarArray[j].nombre  + '</span>';
                contenido += '<p>' + uCadenas.getDiasAgendasAbv(agendasMostrarArray[j].dias) + '</p>';
                contenido += '<input type="hidden" id="hag' + agendasMostrarArray[j].agendaId + '" value="' + agendasMostrarArray[j].dias + '"/>'
                contenido += '</a></li>';
                $('#ulListaResultados').append(contenido);
            }
            $('#ulListaResultados').listview('refresh');
        }
    }, function () {
        $('#txtBusquedaAgenda').blur();
        _loading.off();
        uMensajes.showError('Error de comunicación con el servidor', 1, false);
        $('#btnBuscarAgendas').addClass('ui-disabled');
        $('#resultadoError').show();
    });
}

function buscarEspecialidades() {
    _loading.on('Buscando especialidades...');
    _especialidades.list(function (especialidadesArray) {
        $('#resultadoError').hide();
        var especialidadesMostrarArray = [];
    
        for (var i = 0; i < especialidadesArray.length; i++) {
            var especialidad = {id: 0, nombre: ''};
            especialidad.id = especialidadesArray[i].id;
            especialidad.nombre = especialidadesArray[i].nombre;
            especialidadesMostrarArray.push(especialidad);
        }

        $('#txtBusquedaEspecialidad').blur();
        if (especialidadesMostrarArray.length == 0) {
            _toast.show('Sin resultados', '', '', 'center', 'short');
        } else {
            $('#ulListaResultadosEsp').empty();
            for (var j = 0; j < especialidadesMostrarArray.length; j++) {
                var contenido = '<li><a class="ui-btn ui-btn-a wrap" onclick="elegirEspecialidad(' 
                    + especialidadesMostrarArray[j].id + ')">' 
                    + '<span id="esp' + especialidadesMostrarArray[j].id + '">' + especialidadesMostrarArray[j].nombre  
                    + '</span></a></li>';
                $('#ulListaResultadosEsp').append(contenido);
            }
            $('#ulListaResultadosEsp').listview('refresh');
            _loading.off();
        }
    }, function () {
        $('#resultadoError').show();
        _loading.off();
        uMensajes.showError('Error de comunicación con el servidor', 1, false);
        $('#btnBuscarAgendas').addClass('ui-disabled');
    });
}

function reiniciarBusquedaEspecialidad() {
    _vibrator.on(50);
    $('#dDescBusquedaAgenda').addClass('ui-field-contain');
    $('#ulListaResultadosEsp').empty();
    $('#ulListaResultados').empty();
    $('#cuadroBusquedaAg').hide();
    $('#dResultadosBusquedaAg').hide();
    $('#confirmarBusqueda').hide();
    $('#dNombreEspecialidad').hide();
    $('#ulListaResultadosEsp').show();
    $('#dResultadosBusquedaEsp').show();
    // $('#cuadroBusquedaEsp').show();
    vars.especialidadTemp.id = 0;
    vars.especialidadTemp.nombre = '';
    document.getElementById("btnReiniciarEsp").style.display = "none";
    buscarEspecialidades();
}

function elegirEspecialidad(valor) {
    _vibrator.on(50);
    $('#dDescBusquedaAgenda').removeClass('ui-field-contain');
    $('#ulListaResultadosEsp').hide();
    $('#cuadroBusquedaEsp').hide();
    $('#dNombreEspecialidad').show();
    vars.especialidadTemp.id = valor;
    vars.especialidadTemp.nombre = $('#esp' + valor).text();
    $('#nombreEspecialidad').text($('#esp' + valor).text());
    $('#dResultadosBusquedaAg').show();
    $('#indicacionNoAgendas').hide();
    document.getElementById("btnReiniciarEsp").style.display = "block";
    _loading.on('Buscando agendas...');
    _especialidades.getAgendas(valor, function (agendas) {
        for (var k = 0; k < agendas.length; k++) {
            var agenda = agendas[k];
            var contenido = '<li><a class="ui-btn ui-btn-a wrap" onclick="elegirAgenda(' + agenda.agendaId + ')">' 
                + '<span id="ag' + agenda.agendaId + '">' + agenda.nombre  + '</span>';
            contenido += '<p>' + uCadenas.getDiasAgendasAbv(agenda.dias) + '</p>';
            contenido += '<input type="hidden" id="hag' + agenda.agendaId + '" value="' + agenda.dias + '"/>'
            contenido += '</a></li>';
            $('#ulListaResultados').append(contenido);
        }
        $('#ulListaResultados').listview('refresh');
        _loading.off();
    }, function () {
        _loading.off();
        uMensajes.showError('Error de comunicación con el servidor', 1, false);
        $('#btnBuscarAgendas').addClass('ui-disabled');
    });
}

function elegirAgenda(valor) {
    var yaExiste = false;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        var ptr = vars.pacientesTurno[i];
        if (ptr.pacienteId == vars.pacienteAgenda.pacienteId && ptr.agendaId == valor && valor != 23) {
            yaExiste = true;
            break;
        }
    }
    if (yaExiste) {
        uMensajes.showError('Ya se eligió esta agenda para el paciente ' + vars.pacienteAgenda.pacienteNombre, 2, false);
    } else {
        vars.pacienteAgenda.agendaNombre = $('#ag' + valor).text();
        vars.pacienteAgenda.agendaDias = $('#hag' + valor).val();
        if (valor == 23) {
            navUtil.to('vEstudiosRadiologicos.html', '#pEstudiosRadiologicos', 'slideup', false, function () {
                window.sessionStorage.setItem('_vista', 'estudiosRadiologicos');
                $('#dEstudiosRxApp').show();
                $('#dEstudiosRxTel').hide();
            });
        } else {
            $('#nombreAgenda').text($('#ag' + valor).text());
            confirmarAgenda(valor);
        }
    }
}

function elegirTipoRx(elem) {
    var textoRxElegido = $(elem).children().text();
    vars.pacienteAgenda.observaciones = textoRxElegido;
    var yaExiste = false;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        var ptr = vars.pacientesTurno[i];
        if (ptr.pacienteId == vars.pacienteAgenda.pacienteId && (ptr.agendaId == 23 && ptr.agendaObservaciones == vars.pacienteAgenda.observaciones)) {
            yaExiste = true;
            break;
        }
    }
    if (yaExiste) {
        uMensajes.showError('Ya se eligió esta agenda para el paciente "' + vars.pacienteAgenda.pacienteNombre + '"', 2, false);
    } else {
        // $('#nombreAgenda').text('RADIOLOGÍA (con observaciones)');
        confirmarAgenda(23);
    }
}

function confirmarAgenda(valor) {
    _vibrator.on(50);
    vars.pacienteAgenda.agendaId = valor;
    var pac = {
        agendaId: vars.pacienteAgenda.agendaId,
        agendaTipo: vars.pacienteAgenda.agendaTipo,
        agendaNombre: vars.pacienteAgenda.agendaNombre,
        agendaDias: vars.pacienteAgenda.agendaDias,
        agendaObservaciones: vars.pacienteAgenda.observaciones,
        pacienteId: vars.pacienteAgenda.pacienteId,
        pacienteNombre: vars.pacienteAgenda.pacienteNombre
    };
    vars.pacientesTurno.push(pac);
    _agendas.validarDiasCoincidentes(function () {
        navElegirPaciente(function () {
            for (var i = 0; i < vars.pacientesTurno.length; i++) {
                var num = parseInt($('#totalPac' + vars.pacientesTurno[i].pacienteId).text());
                num++;
                $('#totalPac' + vars.pacientesTurno[i].pacienteId).text(num);
            }
            vars.pacienteAgenda.agendaId = 0;
            vars.pacienteAgenda.agendaTipo = 0;
            vars.pacienteAgenda.agendaNombre = '';
            vars.pacienteAgenda.agendaDias = '';
            vars.pacienteAgenda.observaciones = '';
            vars.pacienteAgenda.pacienteId = 0;
            vars.pacienteAgenda.pacienteNombre = '';
            vars.especialidadTemp.id = 0;
            vars.especialidadTemp.nombre = '';
        }, true, 'pop');
    }, function () {
        vars.pacientesTurno.pop();
        navigator.notification.alert('Los días de la agenda "' + vars.pacienteAgenda.agendaNombre + '" no conciden con todas las agendas elegidas previamente.', null, 'Agenda incompatible', 'Entendido');
    });
}