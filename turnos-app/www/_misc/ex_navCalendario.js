
function navCalendario2(nroQuincena, dir, r, t) {
    if (dir == 0) {
        var desdeString = $('#quincena' + nroQuincena + 'desde').val();
        var hastaString = $('#quincena' + nroQuincena + 'hasta').val();
        vars.quincenaDesde = desdeString;
        vars.quincenaHasta = hastaString;
        vars.nroQuincena = nroQuincena;
    }
    // var quincenaMostrar = $('#quincena' + nroQuincena).text();
    var desdeDate = uFecha.getFechaDate(vars.quincenaDesde);
    var hastaDate = uFecha.getFechaDate(vars.quincenaHasta);

    /**
     * PROCESO DE ENVÍO DE TURNOS AL WEBSERVICE
     * 1) Armar objeto a partir de las elecciones de los usuarios
     * 2) Reiterar la consulta por cada día de la quincena (14 dias)
     * 3) Despues de las 14 consultas, se deberá mostrar el calendario formateado
     * para mostrar los días disponibles con los colores correspondientes
     */
    vars.pacientesTurno.sort(function (a, b) {
        return a.pacienteId - b.pacienteId;
    });

    var idp = -1;
    for (var i = 0; i < vars.pacientesTurno.length; i++) {
        if (idp != vars.pacientesTurno[i].pacienteId) {
            // crear objeto nuevo
            idp = vars.pacientesTurno[i].pacienteId;
            vars.pacientesEnviar.data.push({
                pacId: idp,
                agId: []
            });
        }
        var last = vars.pacientesEnviar.data.pop();
        last.agId.push(vars.pacientesTurno[i].agendaId);
        vars.pacientesEnviar.data.push(last);
    }
    var arrayErrores = [];
    var arrayExito = [];
    _loading.on(vars.quincenaDesde + ' - ' + vars.quincenaHasta + '\n\n'
        + 'Aguarde unos instantes mientras buscamos '
        + 'las mejores fechas para obtener todos los turnos en un mismo día');
    for (var ii = 0; ii < 14; ii++) {
        var fechaEnviar = new Date(desdeDate);
        fechaEnviar.setDate(fechaEnviar.getDate() + ii);
        vars.pacientesEnviar.desde = uFecha.getFechaStringDBsinHora(fechaEnviar);
        var datasend = vars.pacientesEnviar;
        $.ajax({
            url: vars.url + '/disponibilidad',
            data: JSON.stringify(datasend),
            async: false,
            dataType: 'json',
            timeout: 20000,
            method: 'POST',
            contentType: 'application/json',
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 500) {
                    // uMensajes.showError('Fecha: ' + desdeString + '\n' + jqXHR.responseJSON, 1, false);
                    // arrayErrores.push('Fecha: ' + datasend.desde + '\n' + jqXHR.responseJSON + '\n\n');
                    arrayErrores.push(jqXHR.responseJSON);
                }
            },
            success: function (d) {
                var nada = 0;
                for (var f = 0; f < d.length; f++) {
                    for (var g = 0; g < d[f].disponible.length; g++) {
                        arrayExito.push({
                            agendaId: d[f].agendaId,
                            horarioId: d[f].disponible[g].horarioId,
                            hora: d[f].disponible[g].hora,
                            franja: d[f].disponible[g].franja,
                            pacId: d[f].disponible[g].pacId
                        });
                    }
                }
            }
        });
    }

    if (arrayExito.length == 0) {
        _loading.off();
        // navigator.notification.alert('Sin días disponibles. Detalle:\n\n' + mostrarString, null, 'Información', 'Reintentar con otra quincena');
        navigator.notification.alert('No hay días disponibles para la combinación elegida.', function () {
            vars.pacientesEnviar.desde = null;
            vars.pacientesEnviar.data = [];
        }, 'Información', 'Reintentar con otra quincena');
    } else {
        navUtil.to('vCalendario.html', '#pCalendario', t, r, function () {
            // $('#dConfirmarFecha').addClass('ui-disabled');
            StatusBar.backgroundColorByHexString("#0080e0");
            _loading.off();
            window.sessionStorage.setItem('_vista', 'calendario');
            $('.aElegirDia').click(function (e) {
                var idFecha = $(e.currentTarget).attr('id');
                calendario.elegirFecha(idFecha);
            });
            $('#sDesde').text(vars.quincenaDesde);
            $('#sHasta').text(vars.quincenaHasta);
            calendario.cDesde = desdeDate;
            calendario.cHasta = hastaDate;
            calendario.cData = arrayExito;
            calendario.inicializar(desdeDate, hastaDate, arrayExito);
        });
    }
}