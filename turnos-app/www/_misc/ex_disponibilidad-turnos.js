var temp3 = [
    {
        "agenda": "4",
        "nombre": "ONETO CHRISTIAN",
        "tipo": 1,
        "disponible": [
            {
                "fecha": "2018-11-02",
                "libres": [
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-05",
                "libres": [
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-06",
                "libres": [
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-07",
                "libres": [
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "18:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-08",
                "libres": [
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:40:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-09",
                "libres": [
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1015,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 37,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-12",
                "libres": [
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1011,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 33,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-13",
                "libres": [
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "18:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:00:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:20:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "19:40:00"
                    },
                    {
                        "horarioId": 763,
                        "franja": 2,
                        "hora": "20:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-14",
                "libres": [
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1013,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 35,
                        "franja": 2,
                        "hora": "18:00:00"
                    }
                ]
            },
            {
                "fecha": "2018-11-15",
                "libres": [
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "10:00:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "10:20:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "10:40:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:00:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:20:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "11:40:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "12:00:00"
                    },
                    {
                        "horarioId": 1014,
                        "franja": 1,
                        "hora": "12:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "16:40:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:00:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "17:40:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:00:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:20:00"
                    },
                    {
                        "horarioId": 764,
                        "franja": 2,
                        "hora": "18:40:00"
                    }
                ]
            }
        ]
    }
]